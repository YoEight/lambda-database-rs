use std::collections::VecDeque;
use std::iter::Iterator;
use std::mem;

pub struct Node {
    degree: usize,
    keys: Vec<usize>,
    children: Vec<Node>,
}

struct TraverseState<'a> {
    node: &'a Node,
    focus: usize,
    children_completed: bool,
}

pub struct Keys<'a> {
    stack: VecDeque<TraverseState<'a>>,
}

impl<'a> Keys<'a>
{
    fn empty() -> Keys<'static> {
        let stack = VecDeque::new();

        Keys {
            stack,
        }
    }
}

impl<'a> Iterator for Keys<'a>
{
    type Item = usize;

    fn next(&mut self) -> Option<usize> {
        loop {
            if let Some(mut state) = self.stack.pop_front() {
                let node = state.node;
                let cur_focus = state.focus;

                if cur_focus < state.node.keys.len() {
                    if node.is_leaf() {
                        let key = state.node.keys[cur_focus];

                        state.focus += 1;
                        self.stack.push_front(state);

                        return Some(key);
                    } else if state.children_completed {
                        let key = state.node.keys[cur_focus];

                        state.focus += 1;
                        state.children_completed = false;
                        self.stack.push_front(state);

                        return Some(key);
                    } else {
                        let child = &state.node.children[cur_focus];
                        let child_state = TraverseState {
                            node: child,
                            focus: 0,
                            children_completed: false,
                        };

                        state.children_completed = true;
                        self.stack.push_front(state);
                        self.stack.push_front(child_state);
                    }
                } else {
                    if !node.is_leaf() {
                        let child = &node.children[cur_focus];
                        let child_state = TraverseState {
                            node: child,
                            focus: 0,
                            children_completed: false,
                        };

                        self.stack.push_front(child_state);
                    }
                }
            } else {
                break;
            }
        }

        None
    }
}

pub struct DepthFirst<'a> {
    stack: VecDeque<&'a Node>,
}

impl<'a> DepthFirst<'a>
{
    fn empty() -> DepthFirst<'static> {
        let stack = VecDeque::new();

        DepthFirst {
            stack,
        }
    }
}

impl<'a> Iterator for DepthFirst<'a>
{
    type Item = &'a Node;

    fn next(&mut self) -> Option<&'a Node> {
        loop {
            if let Some(node) = self.stack.pop_front() {
                if node.is_leaf() {
                    return Some(node);
                } else {
                    let mut i = node.children.len() - 1;

                    loop {
                        self.stack.push_front(&node.children[i]);

                        if i == 0 {
                            break;
                        }

                        i -= 1;
                    }

                    return Some(node);
                }
            } else {
                break;
            }
        }

        None
    }
}

impl Node {
    pub fn new(degree: usize) -> Node {
        let key_max_len = 2 * degree - 1;

        Node {
            keys: Vec::with_capacity(key_max_len),
            children: Vec::with_capacity(key_max_len + 1),
            degree,
        }
    }

    pub fn is_empty(&self) -> bool {
        self.keys.is_empty()
    }

    pub fn iter_keys<'a>(&'a self) -> Keys<'a> {
        let mut stack = VecDeque::new();
        let state = TraverseState {
            node: self,
            focus: 0,
            children_completed: false,
        };

        stack.push_back(state);

        Keys {
            stack,
        }
    }

    pub fn depth_first<'a>(&'a self) -> DepthFirst<'a> {
        let mut stack = VecDeque::new();

        stack.push_back(self);

        DepthFirst {
            stack,
        }
    }

    pub fn iter_node_keys<'a>(&'a self) -> impl Iterator<Item=usize> + 'a {
        self.keys.iter().map(|k| *k)
    }

    pub fn contains_key(&self, key: usize) -> bool {
        self.keys.contains(&key)
    }

    fn find_key(&self, key: usize) -> usize {
        let mut idx = 0;
        while idx < self.keys.len() && self.keys[idx] < key {
            idx += 1;
        }

        idx
    }

    fn is_full(&self) -> bool {
        self.keys.len() == 2 * self.degree - 1
    }

    fn is_leaf(&self) -> bool {
        self.children.is_empty()
    }

    fn is_key_index_out_of_bound(&self, index: usize) -> bool {
        index > self.keys.len() - 1
    }

    fn push_key(&mut self, key: usize) {
        let mut i = 0;

        for k in self.keys.iter() {
            if key < *k {
                break;
            }

            i += 1;
        }

        self.keys.insert(i, key);
    }

    pub fn traverse(&self) {
        let mut i = 0;

        for key in self.keys.iter() {
            if !self.is_leaf() {
                print!(" [{}]|", i);
                self.children[i].traverse();
                print!("|")
            }

            print!(" <{}> ", key);

            i += 1;
        }

        if !self.is_leaf() {
            // traversing the last child.
            print!(" [{}]|", i);
            self.children[i].traverse();
            print!("|")
        }
    }

    pub fn search(&self, k: usize) -> Option<&Node> {
        let mut i = 0;

        for key in self.keys.iter() {
            if k <= *key {
                break;
            }

            i += 1;
        }

        if i < self.keys.len() && k == self.keys[i] {
            return Some(self);
        }

        if self.is_leaf() {
            None
        } else {
            self.children[i].search(k)
        }
    }

    fn split_child(&mut self, child: usize) {
        let mid_key = self.degree - 1;
        let mut node = self.children.remove(child);
        let mut right = Node::new(node.degree);

        let rest = node
            .keys
            .split_off(mid_key)
            .into_iter()
            .enumerate();

        for (i, v) in rest {
            match i {
                0 => {
                    self.push_key(v);
                },

                _ => {
                    right.keys.push(v);
                },
            }
        }

        self.children.insert(child, node);
        self.children.insert(child + 1, right);
    }

    pub fn insert(&mut self, k: usize) {
        let mut x = self;

        loop {
            if x.is_leaf() {
                break;
            }

            let mut i = 0;
            for key in x.keys.iter() {
                if k <= *key {
                    break;
                }

                i += 1;
            }

            let out_of_bound = x.is_key_index_out_of_bound(i);

            if x.children[i].is_full() {
                x.split_child(i);

                // If true, it means `x` must point to the right part of the
                // newly created splits.
                if out_of_bound {
                    x = &mut x.children[i + 1];
                } else {
                    x = &mut x.children[i];
                }
            } else {
                x = &mut x.children[i];
            }
        }

        x.push_key(k);
    }

    fn has_at_least_degree_keys(&self) -> bool {
        self.keys.len() >= self.degree
    }

    fn get_predecessor(&self, idx: usize) -> usize {
        let mut target = &self.children[idx];

        loop {
            if target.is_leaf() {
                return *target.keys.last().unwrap();
            }

            target = &target.children[target.keys.len()];
        }
    }

    fn get_successor(&self, idx: usize) -> usize {
        let mut target = &self.children[idx+1];

        loop {
            if target.is_leaf() {
                return *target.keys.first().unwrap();
            }

            target = &target.children[0];
        }
    }

    fn get_greatest_smaller_key(&self, key: usize) -> usize {
        let mut result = key;
        let mut target = self;

        loop {
            if target.is_leaf() {
                for k in target.keys.iter() {
                    if *k >= key {
                        break;
                    }

                    result = *k;
                }

                break;
            }

            let mut idx = 0;
            loop {
                if idx >= target.keys.len() || target.keys[idx] >= key {
                    break;
                }

                result = target.keys[idx];
                idx += 1;
            }

            target = &target.children[idx];
        }

        result
    }

    fn get_smallest_bigger_key(&self, key: usize) -> usize {
        let mut result = key;
        let mut target = self;

        loop {
            if target.is_leaf() {
                for k in target.keys.iter() {
                    if *k > key {
                        result = *k;
                        break;
                    }
                }

                break;
            }

            let mut idx = 0;
            loop {
                if idx >= target.keys.len() || target.keys[idx] > key {
                    if idx < target.keys.len() {
                        result = target.keys[idx];
                    }

                    break;
                }

                idx += 1;
            }

            target = &target.children[idx];
        }

        result
    }

    fn delete_key(&mut self, key: usize) {
        for idx in 0..self.keys.len() {
            if self.keys[idx] == key {
                self.keys.remove(idx);
                break;
            }
        }
    }

    fn merge(&mut self, idx: usize) {
        let k = self.keys.remove(idx);
        let right = self.children.remove(idx + 1);

        self.children[idx].keys.push(k);

        for key in right.keys {
            self.children[idx].keys.push(key);
        }

        for child in right.children {
            self.children[idx].children.push(child);
        }
    }

    fn fill(&mut self, idx: usize) {
        if idx != 0 && self.children[idx-1].keys.len() >= self.degree {
            let sibling_key_len = self.children[idx-1].keys.len();
            let sibling_last_key = self.children[idx-1].keys.remove(sibling_key_len-1);
            let parent_key = self.keys[idx-1];

            self.keys[idx-1] = sibling_last_key;
            self.children[idx].keys.insert(0, parent_key);

            if !self.children[idx-1].is_leaf() {
                let sibling_last_child = self.children[idx-1].children.remove(sibling_key_len);
                self.children[idx].children.insert(0, sibling_last_child);
            }
        } else if idx != self.children[idx+1].keys.len() && self.children[idx+1].keys.len() >= self.degree {
            let sibling_first_key = self.children[idx+1].keys.remove(0);
            let parent_key = self.keys[idx];

            self.keys[idx] = sibling_first_key;
            self.children[idx].keys.push(parent_key);

            if !self.children[idx+1].is_leaf() {
                let sibling_first_child = self.children[idx+1].children.remove(0);
                self.children[idx].children.push(sibling_first_child);
            }
        } else {
            if idx != self.keys.len() {
                self.merge(idx);
            } else {
                self.merge(idx-1);
            }
        }
    }

    pub fn delete(&mut self, key: usize) {
        let degree = self.degree;
        let mut target = self;

        loop {
            if target.is_leaf() {
                target.delete_key(key);
                break;
            }

            let idx = target.find_key(key);
            if idx < target.keys.len() && target.keys[idx] == key {
                if target.children[idx].has_at_least_degree_keys() {
                    let k0 = target.get_predecessor(idx);
                    target.children[idx].delete(k0);
                    target.keys[idx] = k0;
                } else if target.children[idx+1].has_at_least_degree_keys() {
                    let k0 = target.get_successor(idx);
                    target.children[idx+1].delete(k0);
                    target.keys[idx] = k0;
                } else {
                    target.merge(idx);
                    target.children[idx].delete(key);
                }

                break;
            } else {
                if target.children[idx].keys.len() < degree {
                    target.fill(idx);
                }

                target = &mut target.children[idx];
            }
        }
    }
}

pub struct BTree {
    root: Option<Node>,
    degree: usize,
}

impl BTree {
    pub fn new(degree: usize) -> BTree {
        BTree {
            root: None,
            degree,
        }
    }

    pub fn traverse(&self) {
        self.root.iter().for_each(|node| node.traverse())
    }

    pub fn search(&self, key: usize) -> Option<&Node> {
        self.root.as_ref().and_then(|node| node.search(key))
    }

    pub fn insert(&mut self, key: usize) {
        if self.root.is_none() {
            self.root = Some(Node::new(self.degree));
        }

        self.root.as_mut().into_iter().for_each(|root| {
            if root.is_full() {
                let new_root = Node::new(root.degree);
                let child = mem::replace(root, new_root);

                root.children.push(child);
                root.split_child(0);
            }

            root.insert(key);
        });
    }

    pub fn keys<'a>(&'a self) -> Keys<'a> {
        if let Some(node) = self.root.as_ref() {
            node.iter_keys()
        } else {
            Keys::empty()
        }
    }

    pub fn depth_first<'a>(&'a self) -> DepthFirst<'a> {
        if let Some(node) = self.root.as_ref() {
            node.depth_first()
        } else {
            DepthFirst::empty()
        }
    }

    pub fn get_greatest_smaller_key(&self, key: usize) -> usize {
        if let Some(node) = self.root.as_ref() {
            node.get_greatest_smaller_key(key)
        } else {
            key
        }
    }

    pub fn get_predecessor(&self, idx: usize) -> Option<usize> {
        self.root.as_ref().map(|node| node.get_predecessor(idx))
    }

    pub fn get_successor(&self, idx: usize) -> Option<usize> {
        self.root.as_ref().map(|node| node.get_successor(idx))
    }

    pub fn get_smallest_bigger_key(&self, key: usize) -> usize {
        if let Some(node) = self.root.as_ref() {
            node.get_smallest_bigger_key(key)
        } else {
            key
        }
    }

    pub fn delete(&mut self, key: usize) {
        if let Some(node) = self.root.as_mut() {
            node.delete(key);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_not_found() {
        let root = BTree::new(3);

        let result = root.search(1);

        assert_eq!(result.is_none(), true);
    }

    #[test]
    fn test_found() {
        let mut root = BTree::new(3);

        root.insert(10);

        let result = root.search(10);

        assert_eq!(result.is_some(), true);

        let node = result.unwrap();

        assert_eq!(node.contains_key(10), true);
    }

    #[test]
    fn test_usecase_1() {
        let mut root = BTree::new(3);

        root.insert(10);
        root.insert(20);
        root.insert(30);
        root.insert(40);
        root.insert(50);
        root.insert(60);
        root.insert(70);
        root.insert(80);
        root.insert(90);

        let result = root.search(10);

        assert_eq!(result.is_some(), true);

        let node = result.unwrap();

        for key in vec![10, 20].into_iter() {
            assert_eq!(node.contains_key(key), true);
        }

        let result = root.search(30);

        assert_eq!(result.is_some(), true);

        let node = result.unwrap();

        for key in vec![30, 60].into_iter() {
            assert_eq!(node.contains_key(key), true);
        }

        let result = root.search(40);

        assert_eq!(result.is_some(), true);

        let node = result.unwrap();

        for key in vec![40, 50].into_iter() {
            assert_eq!(node.contains_key(key), true);
        }

        let result = root.search(70);

        assert_eq!(result.is_some(), true);

        let node = result.unwrap();

        for key in vec![70, 80, 90].into_iter() {
            assert_eq!(node.contains_key(key), true);
        }
    }

    #[test]
    fn test_usecase_1_iterator_keys() {
        let mut root = BTree::new(3);

        root.insert(10);
        root.insert(20);
        root.insert(30);
        root.insert(40);
        root.insert(50);
        root.insert(60);
        root.insert(70);
        root.insert(80);
        root.insert(90);


        print!("traverse: ");
        root.traverse();
        print!("\niterator: ");
        root.keys().for_each(|key| print!(" {}", key));
    }

    #[test]
    fn test_usecase_1_iterator_depth_first() {
        let mut root = BTree::new(3);

        root.insert(10);
        root.insert(20);
        root.insert(30);
        root.insert(40);
        root.insert(50);
        root.insert(60);
        root.insert(70);
        root.insert(80);
        root.insert(90);


        println!("Printing nodes");
        root.depth_first().for_each(|node| {
            println!("");
            node.iter_node_keys().for_each(|key| print!(" {}", key));
        });
    }
}
