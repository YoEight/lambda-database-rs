// This file is generated. Do not edit
// @generated

// https://github.com/Manishearth/rust-clippy/issues/702
#![allow(unknown_lints)]
#![allow(clippy)]

#![cfg_attr(rustfmt, rustfmt_skip)]

#![allow(box_pointers)]
#![allow(dead_code)]
#![allow(missing_docs)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(trivial_casts)]
#![allow(unsafe_code)]
#![allow(unused_imports)]
#![allow(unused_results)]

use protobuf::Message as Message_imported_for_functions;
use protobuf::ProtobufEnum as ProtobufEnum_imported_for_functions;

#[derive(PartialEq,Clone,Default)]
pub struct NewEvent {
    // message fields
    event_id: ::std::option::Option<::bytes::Bytes>,
    event_type: ::std::option::Option<::protobuf::chars::Chars>,
    data_content_type: ::std::option::Option<i32>,
    metadata_content_type: ::std::option::Option<i32>,
    data: ::std::option::Option<::bytes::Bytes>,
    metadata: ::std::option::Option<::bytes::Bytes>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for NewEvent {}

impl NewEvent {
    pub fn new() -> NewEvent {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static NewEvent {
        static mut instance: ::protobuf::lazy::Lazy<NewEvent> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const NewEvent,
        };
        unsafe {
            instance.get(NewEvent::new)
        }
    }

    // required bytes event_id = 1;

    pub fn clear_event_id(&mut self) {
        self.event_id = ::std::option::Option::None;
    }

    pub fn has_event_id(&self) -> bool {
        self.event_id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_event_id(&mut self, v: ::bytes::Bytes) {
        self.event_id = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_event_id(&mut self) -> &mut ::bytes::Bytes {
        if self.event_id.is_none() {
            self.event_id = ::std::option::Option::Some(::bytes::Bytes::new());
        }
        self.event_id.as_mut().unwrap()
    }

    // Take field
    pub fn take_event_id(&mut self) -> ::bytes::Bytes {
        self.event_id.take().unwrap_or_else(|| ::bytes::Bytes::new())
    }

    pub fn get_event_id(&self) -> &[u8] {
        match self.event_id.as_ref() {
            Some(v) => v,
            None => &[],
        }
    }

    fn get_event_id_for_reflect(&self) -> &::std::option::Option<::bytes::Bytes> {
        &self.event_id
    }

    fn mut_event_id_for_reflect(&mut self) -> &mut ::std::option::Option<::bytes::Bytes> {
        &mut self.event_id
    }

    // required string event_type = 2;

    pub fn clear_event_type(&mut self) {
        self.event_type = ::std::option::Option::None;
    }

    pub fn has_event_type(&self) -> bool {
        self.event_type.is_some()
    }

    // Param is passed by value, moved
    pub fn set_event_type(&mut self, v: ::protobuf::chars::Chars) {
        self.event_type = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_event_type(&mut self) -> &mut ::protobuf::chars::Chars {
        if self.event_type.is_none() {
            self.event_type = ::std::option::Option::Some(::protobuf::chars::Chars::new());
        }
        self.event_type.as_mut().unwrap()
    }

    // Take field
    pub fn take_event_type(&mut self) -> ::protobuf::chars::Chars {
        self.event_type.take().unwrap_or_else(|| ::protobuf::chars::Chars::new())
    }

    pub fn get_event_type(&self) -> &str {
        match self.event_type.as_ref() {
            Some(v) => v,
            None => "",
        }
    }

    fn get_event_type_for_reflect(&self) -> &::std::option::Option<::protobuf::chars::Chars> {
        &self.event_type
    }

    fn mut_event_type_for_reflect(&mut self) -> &mut ::std::option::Option<::protobuf::chars::Chars> {
        &mut self.event_type
    }

    // required int32 data_content_type = 3;

    pub fn clear_data_content_type(&mut self) {
        self.data_content_type = ::std::option::Option::None;
    }

    pub fn has_data_content_type(&self) -> bool {
        self.data_content_type.is_some()
    }

    // Param is passed by value, moved
    pub fn set_data_content_type(&mut self, v: i32) {
        self.data_content_type = ::std::option::Option::Some(v);
    }

    pub fn get_data_content_type(&self) -> i32 {
        self.data_content_type.unwrap_or(0)
    }

    fn get_data_content_type_for_reflect(&self) -> &::std::option::Option<i32> {
        &self.data_content_type
    }

    fn mut_data_content_type_for_reflect(&mut self) -> &mut ::std::option::Option<i32> {
        &mut self.data_content_type
    }

    // required int32 metadata_content_type = 4;

    pub fn clear_metadata_content_type(&mut self) {
        self.metadata_content_type = ::std::option::Option::None;
    }

    pub fn has_metadata_content_type(&self) -> bool {
        self.metadata_content_type.is_some()
    }

    // Param is passed by value, moved
    pub fn set_metadata_content_type(&mut self, v: i32) {
        self.metadata_content_type = ::std::option::Option::Some(v);
    }

    pub fn get_metadata_content_type(&self) -> i32 {
        self.metadata_content_type.unwrap_or(0)
    }

    fn get_metadata_content_type_for_reflect(&self) -> &::std::option::Option<i32> {
        &self.metadata_content_type
    }

    fn mut_metadata_content_type_for_reflect(&mut self) -> &mut ::std::option::Option<i32> {
        &mut self.metadata_content_type
    }

    // required bytes data = 5;

    pub fn clear_data(&mut self) {
        self.data = ::std::option::Option::None;
    }

    pub fn has_data(&self) -> bool {
        self.data.is_some()
    }

    // Param is passed by value, moved
    pub fn set_data(&mut self, v: ::bytes::Bytes) {
        self.data = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_data(&mut self) -> &mut ::bytes::Bytes {
        if self.data.is_none() {
            self.data = ::std::option::Option::Some(::bytes::Bytes::new());
        }
        self.data.as_mut().unwrap()
    }

    // Take field
    pub fn take_data(&mut self) -> ::bytes::Bytes {
        self.data.take().unwrap_or_else(|| ::bytes::Bytes::new())
    }

    pub fn get_data(&self) -> &[u8] {
        match self.data.as_ref() {
            Some(v) => v,
            None => &[],
        }
    }

    fn get_data_for_reflect(&self) -> &::std::option::Option<::bytes::Bytes> {
        &self.data
    }

    fn mut_data_for_reflect(&mut self) -> &mut ::std::option::Option<::bytes::Bytes> {
        &mut self.data
    }

    // optional bytes metadata = 6;

    pub fn clear_metadata(&mut self) {
        self.metadata = ::std::option::Option::None;
    }

    pub fn has_metadata(&self) -> bool {
        self.metadata.is_some()
    }

    // Param is passed by value, moved
    pub fn set_metadata(&mut self, v: ::bytes::Bytes) {
        self.metadata = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_metadata(&mut self) -> &mut ::bytes::Bytes {
        if self.metadata.is_none() {
            self.metadata = ::std::option::Option::Some(::bytes::Bytes::new());
        }
        self.metadata.as_mut().unwrap()
    }

    // Take field
    pub fn take_metadata(&mut self) -> ::bytes::Bytes {
        self.metadata.take().unwrap_or_else(|| ::bytes::Bytes::new())
    }

    pub fn get_metadata(&self) -> &[u8] {
        match self.metadata.as_ref() {
            Some(v) => v,
            None => &[],
        }
    }

    fn get_metadata_for_reflect(&self) -> &::std::option::Option<::bytes::Bytes> {
        &self.metadata
    }

    fn mut_metadata_for_reflect(&mut self) -> &mut ::std::option::Option<::bytes::Bytes> {
        &mut self.metadata
    }
}

impl ::protobuf::Message for NewEvent {
    fn is_initialized(&self) -> bool {
        if self.event_id.is_none() {
            return false;
        }
        if self.event_type.is_none() {
            return false;
        }
        if self.data_content_type.is_none() {
            return false;
        }
        if self.metadata_content_type.is_none() {
            return false;
        }
        if self.data.is_none() {
            return false;
        }
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_carllerche_bytes_into(wire_type, is, &mut self.event_id)?;
                },
                2 => {
                    ::protobuf::rt::read_singular_carllerche_string_into(wire_type, is, &mut self.event_type)?;
                },
                3 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int32()?;
                    self.data_content_type = ::std::option::Option::Some(tmp);
                },
                4 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int32()?;
                    self.metadata_content_type = ::std::option::Option::Some(tmp);
                },
                5 => {
                    ::protobuf::rt::read_singular_carllerche_bytes_into(wire_type, is, &mut self.data)?;
                },
                6 => {
                    ::protobuf::rt::read_singular_carllerche_bytes_into(wire_type, is, &mut self.metadata)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(ref v) = self.event_id.as_ref() {
            my_size += ::protobuf::rt::bytes_size(1, &v);
        }
        if let Some(ref v) = self.event_type.as_ref() {
            my_size += ::protobuf::rt::string_size(2, &v);
        }
        if let Some(v) = self.data_content_type {
            my_size += ::protobuf::rt::value_size(3, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(v) = self.metadata_content_type {
            my_size += ::protobuf::rt::value_size(4, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(ref v) = self.data.as_ref() {
            my_size += ::protobuf::rt::bytes_size(5, &v);
        }
        if let Some(ref v) = self.metadata.as_ref() {
            my_size += ::protobuf::rt::bytes_size(6, &v);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(ref v) = self.event_id.as_ref() {
            os.write_bytes(1, v)?;
        }
        if let Some(ref v) = self.event_type.as_ref() {
            os.write_string(2, v)?;
        }
        if let Some(v) = self.data_content_type {
            os.write_int32(3, v)?;
        }
        if let Some(v) = self.metadata_content_type {
            os.write_int32(4, v)?;
        }
        if let Some(ref v) = self.data.as_ref() {
            os.write_bytes(5, v)?;
        }
        if let Some(ref v) = self.metadata.as_ref() {
            os.write_bytes(6, v)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for NewEvent {
    fn new() -> NewEvent {
        NewEvent::new()
    }

    fn descriptor_static(_: ::std::option::Option<NewEvent>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheBytes>(
                    "event_id",
                    NewEvent::get_event_id_for_reflect,
                    NewEvent::mut_event_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheChars>(
                    "event_type",
                    NewEvent::get_event_type_for_reflect,
                    NewEvent::mut_event_type_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt32>(
                    "data_content_type",
                    NewEvent::get_data_content_type_for_reflect,
                    NewEvent::mut_data_content_type_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt32>(
                    "metadata_content_type",
                    NewEvent::get_metadata_content_type_for_reflect,
                    NewEvent::mut_metadata_content_type_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheBytes>(
                    "data",
                    NewEvent::get_data_for_reflect,
                    NewEvent::mut_data_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheBytes>(
                    "metadata",
                    NewEvent::get_metadata_for_reflect,
                    NewEvent::mut_metadata_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<NewEvent>(
                    "NewEvent",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for NewEvent {
    fn clear(&mut self) {
        self.clear_event_id();
        self.clear_event_type();
        self.clear_data_content_type();
        self.clear_metadata_content_type();
        self.clear_data();
        self.clear_metadata();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for NewEvent {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for NewEvent {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct EventRecord {
    // message fields
    event_stream_id: ::std::option::Option<::protobuf::chars::Chars>,
    event_number: ::std::option::Option<i64>,
    event_id: ::std::option::Option<::bytes::Bytes>,
    event_type: ::std::option::Option<::protobuf::chars::Chars>,
    data_content_type: ::std::option::Option<i32>,
    metadata_content_type: ::std::option::Option<i32>,
    data: ::std::option::Option<::bytes::Bytes>,
    metadata: ::std::option::Option<::bytes::Bytes>,
    created: ::std::option::Option<i64>,
    created_epoch: ::std::option::Option<i64>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for EventRecord {}

impl EventRecord {
    pub fn new() -> EventRecord {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static EventRecord {
        static mut instance: ::protobuf::lazy::Lazy<EventRecord> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const EventRecord,
        };
        unsafe {
            instance.get(EventRecord::new)
        }
    }

    // required string event_stream_id = 1;

    pub fn clear_event_stream_id(&mut self) {
        self.event_stream_id = ::std::option::Option::None;
    }

    pub fn has_event_stream_id(&self) -> bool {
        self.event_stream_id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_event_stream_id(&mut self, v: ::protobuf::chars::Chars) {
        self.event_stream_id = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_event_stream_id(&mut self) -> &mut ::protobuf::chars::Chars {
        if self.event_stream_id.is_none() {
            self.event_stream_id = ::std::option::Option::Some(::protobuf::chars::Chars::new());
        }
        self.event_stream_id.as_mut().unwrap()
    }

    // Take field
    pub fn take_event_stream_id(&mut self) -> ::protobuf::chars::Chars {
        self.event_stream_id.take().unwrap_or_else(|| ::protobuf::chars::Chars::new())
    }

    pub fn get_event_stream_id(&self) -> &str {
        match self.event_stream_id.as_ref() {
            Some(v) => v,
            None => "",
        }
    }

    fn get_event_stream_id_for_reflect(&self) -> &::std::option::Option<::protobuf::chars::Chars> {
        &self.event_stream_id
    }

    fn mut_event_stream_id_for_reflect(&mut self) -> &mut ::std::option::Option<::protobuf::chars::Chars> {
        &mut self.event_stream_id
    }

    // required int64 event_number = 2;

    pub fn clear_event_number(&mut self) {
        self.event_number = ::std::option::Option::None;
    }

    pub fn has_event_number(&self) -> bool {
        self.event_number.is_some()
    }

    // Param is passed by value, moved
    pub fn set_event_number(&mut self, v: i64) {
        self.event_number = ::std::option::Option::Some(v);
    }

    pub fn get_event_number(&self) -> i64 {
        self.event_number.unwrap_or(0)
    }

    fn get_event_number_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.event_number
    }

    fn mut_event_number_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.event_number
    }

    // required bytes event_id = 3;

    pub fn clear_event_id(&mut self) {
        self.event_id = ::std::option::Option::None;
    }

    pub fn has_event_id(&self) -> bool {
        self.event_id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_event_id(&mut self, v: ::bytes::Bytes) {
        self.event_id = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_event_id(&mut self) -> &mut ::bytes::Bytes {
        if self.event_id.is_none() {
            self.event_id = ::std::option::Option::Some(::bytes::Bytes::new());
        }
        self.event_id.as_mut().unwrap()
    }

    // Take field
    pub fn take_event_id(&mut self) -> ::bytes::Bytes {
        self.event_id.take().unwrap_or_else(|| ::bytes::Bytes::new())
    }

    pub fn get_event_id(&self) -> &[u8] {
        match self.event_id.as_ref() {
            Some(v) => v,
            None => &[],
        }
    }

    fn get_event_id_for_reflect(&self) -> &::std::option::Option<::bytes::Bytes> {
        &self.event_id
    }

    fn mut_event_id_for_reflect(&mut self) -> &mut ::std::option::Option<::bytes::Bytes> {
        &mut self.event_id
    }

    // required string event_type = 4;

    pub fn clear_event_type(&mut self) {
        self.event_type = ::std::option::Option::None;
    }

    pub fn has_event_type(&self) -> bool {
        self.event_type.is_some()
    }

    // Param is passed by value, moved
    pub fn set_event_type(&mut self, v: ::protobuf::chars::Chars) {
        self.event_type = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_event_type(&mut self) -> &mut ::protobuf::chars::Chars {
        if self.event_type.is_none() {
            self.event_type = ::std::option::Option::Some(::protobuf::chars::Chars::new());
        }
        self.event_type.as_mut().unwrap()
    }

    // Take field
    pub fn take_event_type(&mut self) -> ::protobuf::chars::Chars {
        self.event_type.take().unwrap_or_else(|| ::protobuf::chars::Chars::new())
    }

    pub fn get_event_type(&self) -> &str {
        match self.event_type.as_ref() {
            Some(v) => v,
            None => "",
        }
    }

    fn get_event_type_for_reflect(&self) -> &::std::option::Option<::protobuf::chars::Chars> {
        &self.event_type
    }

    fn mut_event_type_for_reflect(&mut self) -> &mut ::std::option::Option<::protobuf::chars::Chars> {
        &mut self.event_type
    }

    // required int32 data_content_type = 5;

    pub fn clear_data_content_type(&mut self) {
        self.data_content_type = ::std::option::Option::None;
    }

    pub fn has_data_content_type(&self) -> bool {
        self.data_content_type.is_some()
    }

    // Param is passed by value, moved
    pub fn set_data_content_type(&mut self, v: i32) {
        self.data_content_type = ::std::option::Option::Some(v);
    }

    pub fn get_data_content_type(&self) -> i32 {
        self.data_content_type.unwrap_or(0)
    }

    fn get_data_content_type_for_reflect(&self) -> &::std::option::Option<i32> {
        &self.data_content_type
    }

    fn mut_data_content_type_for_reflect(&mut self) -> &mut ::std::option::Option<i32> {
        &mut self.data_content_type
    }

    // required int32 metadata_content_type = 6;

    pub fn clear_metadata_content_type(&mut self) {
        self.metadata_content_type = ::std::option::Option::None;
    }

    pub fn has_metadata_content_type(&self) -> bool {
        self.metadata_content_type.is_some()
    }

    // Param is passed by value, moved
    pub fn set_metadata_content_type(&mut self, v: i32) {
        self.metadata_content_type = ::std::option::Option::Some(v);
    }

    pub fn get_metadata_content_type(&self) -> i32 {
        self.metadata_content_type.unwrap_or(0)
    }

    fn get_metadata_content_type_for_reflect(&self) -> &::std::option::Option<i32> {
        &self.metadata_content_type
    }

    fn mut_metadata_content_type_for_reflect(&mut self) -> &mut ::std::option::Option<i32> {
        &mut self.metadata_content_type
    }

    // required bytes data = 7;

    pub fn clear_data(&mut self) {
        self.data = ::std::option::Option::None;
    }

    pub fn has_data(&self) -> bool {
        self.data.is_some()
    }

    // Param is passed by value, moved
    pub fn set_data(&mut self, v: ::bytes::Bytes) {
        self.data = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_data(&mut self) -> &mut ::bytes::Bytes {
        if self.data.is_none() {
            self.data = ::std::option::Option::Some(::bytes::Bytes::new());
        }
        self.data.as_mut().unwrap()
    }

    // Take field
    pub fn take_data(&mut self) -> ::bytes::Bytes {
        self.data.take().unwrap_or_else(|| ::bytes::Bytes::new())
    }

    pub fn get_data(&self) -> &[u8] {
        match self.data.as_ref() {
            Some(v) => v,
            None => &[],
        }
    }

    fn get_data_for_reflect(&self) -> &::std::option::Option<::bytes::Bytes> {
        &self.data
    }

    fn mut_data_for_reflect(&mut self) -> &mut ::std::option::Option<::bytes::Bytes> {
        &mut self.data
    }

    // optional bytes metadata = 8;

    pub fn clear_metadata(&mut self) {
        self.metadata = ::std::option::Option::None;
    }

    pub fn has_metadata(&self) -> bool {
        self.metadata.is_some()
    }

    // Param is passed by value, moved
    pub fn set_metadata(&mut self, v: ::bytes::Bytes) {
        self.metadata = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_metadata(&mut self) -> &mut ::bytes::Bytes {
        if self.metadata.is_none() {
            self.metadata = ::std::option::Option::Some(::bytes::Bytes::new());
        }
        self.metadata.as_mut().unwrap()
    }

    // Take field
    pub fn take_metadata(&mut self) -> ::bytes::Bytes {
        self.metadata.take().unwrap_or_else(|| ::bytes::Bytes::new())
    }

    pub fn get_metadata(&self) -> &[u8] {
        match self.metadata.as_ref() {
            Some(v) => v,
            None => &[],
        }
    }

    fn get_metadata_for_reflect(&self) -> &::std::option::Option<::bytes::Bytes> {
        &self.metadata
    }

    fn mut_metadata_for_reflect(&mut self) -> &mut ::std::option::Option<::bytes::Bytes> {
        &mut self.metadata
    }

    // optional int64 created = 9;

    pub fn clear_created(&mut self) {
        self.created = ::std::option::Option::None;
    }

    pub fn has_created(&self) -> bool {
        self.created.is_some()
    }

    // Param is passed by value, moved
    pub fn set_created(&mut self, v: i64) {
        self.created = ::std::option::Option::Some(v);
    }

    pub fn get_created(&self) -> i64 {
        self.created.unwrap_or(0)
    }

    fn get_created_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.created
    }

    fn mut_created_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.created
    }

    // optional int64 created_epoch = 10;

    pub fn clear_created_epoch(&mut self) {
        self.created_epoch = ::std::option::Option::None;
    }

    pub fn has_created_epoch(&self) -> bool {
        self.created_epoch.is_some()
    }

    // Param is passed by value, moved
    pub fn set_created_epoch(&mut self, v: i64) {
        self.created_epoch = ::std::option::Option::Some(v);
    }

    pub fn get_created_epoch(&self) -> i64 {
        self.created_epoch.unwrap_or(0)
    }

    fn get_created_epoch_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.created_epoch
    }

    fn mut_created_epoch_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.created_epoch
    }
}

impl ::protobuf::Message for EventRecord {
    fn is_initialized(&self) -> bool {
        if self.event_stream_id.is_none() {
            return false;
        }
        if self.event_number.is_none() {
            return false;
        }
        if self.event_id.is_none() {
            return false;
        }
        if self.event_type.is_none() {
            return false;
        }
        if self.data_content_type.is_none() {
            return false;
        }
        if self.metadata_content_type.is_none() {
            return false;
        }
        if self.data.is_none() {
            return false;
        }
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_carllerche_string_into(wire_type, is, &mut self.event_stream_id)?;
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.event_number = ::std::option::Option::Some(tmp);
                },
                3 => {
                    ::protobuf::rt::read_singular_carllerche_bytes_into(wire_type, is, &mut self.event_id)?;
                },
                4 => {
                    ::protobuf::rt::read_singular_carllerche_string_into(wire_type, is, &mut self.event_type)?;
                },
                5 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int32()?;
                    self.data_content_type = ::std::option::Option::Some(tmp);
                },
                6 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int32()?;
                    self.metadata_content_type = ::std::option::Option::Some(tmp);
                },
                7 => {
                    ::protobuf::rt::read_singular_carllerche_bytes_into(wire_type, is, &mut self.data)?;
                },
                8 => {
                    ::protobuf::rt::read_singular_carllerche_bytes_into(wire_type, is, &mut self.metadata)?;
                },
                9 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.created = ::std::option::Option::Some(tmp);
                },
                10 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.created_epoch = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(ref v) = self.event_stream_id.as_ref() {
            my_size += ::protobuf::rt::string_size(1, &v);
        }
        if let Some(v) = self.event_number {
            my_size += ::protobuf::rt::value_size(2, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(ref v) = self.event_id.as_ref() {
            my_size += ::protobuf::rt::bytes_size(3, &v);
        }
        if let Some(ref v) = self.event_type.as_ref() {
            my_size += ::protobuf::rt::string_size(4, &v);
        }
        if let Some(v) = self.data_content_type {
            my_size += ::protobuf::rt::value_size(5, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(v) = self.metadata_content_type {
            my_size += ::protobuf::rt::value_size(6, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(ref v) = self.data.as_ref() {
            my_size += ::protobuf::rt::bytes_size(7, &v);
        }
        if let Some(ref v) = self.metadata.as_ref() {
            my_size += ::protobuf::rt::bytes_size(8, &v);
        }
        if let Some(v) = self.created {
            my_size += ::protobuf::rt::value_size(9, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(v) = self.created_epoch {
            my_size += ::protobuf::rt::value_size(10, v, ::protobuf::wire_format::WireTypeVarint);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(ref v) = self.event_stream_id.as_ref() {
            os.write_string(1, v)?;
        }
        if let Some(v) = self.event_number {
            os.write_int64(2, v)?;
        }
        if let Some(ref v) = self.event_id.as_ref() {
            os.write_bytes(3, v)?;
        }
        if let Some(ref v) = self.event_type.as_ref() {
            os.write_string(4, v)?;
        }
        if let Some(v) = self.data_content_type {
            os.write_int32(5, v)?;
        }
        if let Some(v) = self.metadata_content_type {
            os.write_int32(6, v)?;
        }
        if let Some(ref v) = self.data.as_ref() {
            os.write_bytes(7, v)?;
        }
        if let Some(ref v) = self.metadata.as_ref() {
            os.write_bytes(8, v)?;
        }
        if let Some(v) = self.created {
            os.write_int64(9, v)?;
        }
        if let Some(v) = self.created_epoch {
            os.write_int64(10, v)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for EventRecord {
    fn new() -> EventRecord {
        EventRecord::new()
    }

    fn descriptor_static(_: ::std::option::Option<EventRecord>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheChars>(
                    "event_stream_id",
                    EventRecord::get_event_stream_id_for_reflect,
                    EventRecord::mut_event_stream_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "event_number",
                    EventRecord::get_event_number_for_reflect,
                    EventRecord::mut_event_number_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheBytes>(
                    "event_id",
                    EventRecord::get_event_id_for_reflect,
                    EventRecord::mut_event_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheChars>(
                    "event_type",
                    EventRecord::get_event_type_for_reflect,
                    EventRecord::mut_event_type_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt32>(
                    "data_content_type",
                    EventRecord::get_data_content_type_for_reflect,
                    EventRecord::mut_data_content_type_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt32>(
                    "metadata_content_type",
                    EventRecord::get_metadata_content_type_for_reflect,
                    EventRecord::mut_metadata_content_type_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheBytes>(
                    "data",
                    EventRecord::get_data_for_reflect,
                    EventRecord::mut_data_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheBytes>(
                    "metadata",
                    EventRecord::get_metadata_for_reflect,
                    EventRecord::mut_metadata_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "created",
                    EventRecord::get_created_for_reflect,
                    EventRecord::mut_created_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "created_epoch",
                    EventRecord::get_created_epoch_for_reflect,
                    EventRecord::mut_created_epoch_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<EventRecord>(
                    "EventRecord",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for EventRecord {
    fn clear(&mut self) {
        self.clear_event_stream_id();
        self.clear_event_number();
        self.clear_event_id();
        self.clear_event_type();
        self.clear_data_content_type();
        self.clear_metadata_content_type();
        self.clear_data();
        self.clear_metadata();
        self.clear_created();
        self.clear_created_epoch();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for EventRecord {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for EventRecord {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct ResolvedIndexedEvent {
    // message fields
    event: ::protobuf::SingularPtrField<EventRecord>,
    link: ::protobuf::SingularPtrField<EventRecord>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for ResolvedIndexedEvent {}

impl ResolvedIndexedEvent {
    pub fn new() -> ResolvedIndexedEvent {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static ResolvedIndexedEvent {
        static mut instance: ::protobuf::lazy::Lazy<ResolvedIndexedEvent> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ResolvedIndexedEvent,
        };
        unsafe {
            instance.get(ResolvedIndexedEvent::new)
        }
    }

    // required .Messages.EventRecord event = 1;

    pub fn clear_event(&mut self) {
        self.event.clear();
    }

    pub fn has_event(&self) -> bool {
        self.event.is_some()
    }

    // Param is passed by value, moved
    pub fn set_event(&mut self, v: EventRecord) {
        self.event = ::protobuf::SingularPtrField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_event(&mut self) -> &mut EventRecord {
        if self.event.is_none() {
            self.event.set_default();
        }
        self.event.as_mut().unwrap()
    }

    // Take field
    pub fn take_event(&mut self) -> EventRecord {
        self.event.take().unwrap_or_else(|| EventRecord::new())
    }

    pub fn get_event(&self) -> &EventRecord {
        self.event.as_ref().unwrap_or_else(|| EventRecord::default_instance())
    }

    fn get_event_for_reflect(&self) -> &::protobuf::SingularPtrField<EventRecord> {
        &self.event
    }

    fn mut_event_for_reflect(&mut self) -> &mut ::protobuf::SingularPtrField<EventRecord> {
        &mut self.event
    }

    // optional .Messages.EventRecord link = 2;

    pub fn clear_link(&mut self) {
        self.link.clear();
    }

    pub fn has_link(&self) -> bool {
        self.link.is_some()
    }

    // Param is passed by value, moved
    pub fn set_link(&mut self, v: EventRecord) {
        self.link = ::protobuf::SingularPtrField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_link(&mut self) -> &mut EventRecord {
        if self.link.is_none() {
            self.link.set_default();
        }
        self.link.as_mut().unwrap()
    }

    // Take field
    pub fn take_link(&mut self) -> EventRecord {
        self.link.take().unwrap_or_else(|| EventRecord::new())
    }

    pub fn get_link(&self) -> &EventRecord {
        self.link.as_ref().unwrap_or_else(|| EventRecord::default_instance())
    }

    fn get_link_for_reflect(&self) -> &::protobuf::SingularPtrField<EventRecord> {
        &self.link
    }

    fn mut_link_for_reflect(&mut self) -> &mut ::protobuf::SingularPtrField<EventRecord> {
        &mut self.link
    }
}

impl ::protobuf::Message for ResolvedIndexedEvent {
    fn is_initialized(&self) -> bool {
        if self.event.is_none() {
            return false;
        }
        for v in &self.event {
            if !v.is_initialized() {
                return false;
            }
        };
        for v in &self.link {
            if !v.is_initialized() {
                return false;
            }
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_message_into(wire_type, is, &mut self.event)?;
                },
                2 => {
                    ::protobuf::rt::read_singular_message_into(wire_type, is, &mut self.link)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(ref v) = self.event.as_ref() {
            let len = v.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        }
        if let Some(ref v) = self.link.as_ref() {
            let len = v.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(ref v) = self.event.as_ref() {
            os.write_tag(1, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        }
        if let Some(ref v) = self.link.as_ref() {
            os.write_tag(2, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for ResolvedIndexedEvent {
    fn new() -> ResolvedIndexedEvent {
        ResolvedIndexedEvent::new()
    }

    fn descriptor_static(_: ::std::option::Option<ResolvedIndexedEvent>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_singular_ptr_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<EventRecord>>(
                    "event",
                    ResolvedIndexedEvent::get_event_for_reflect,
                    ResolvedIndexedEvent::mut_event_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_ptr_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<EventRecord>>(
                    "link",
                    ResolvedIndexedEvent::get_link_for_reflect,
                    ResolvedIndexedEvent::mut_link_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<ResolvedIndexedEvent>(
                    "ResolvedIndexedEvent",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for ResolvedIndexedEvent {
    fn clear(&mut self) {
        self.clear_event();
        self.clear_link();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for ResolvedIndexedEvent {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for ResolvedIndexedEvent {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct ResolvedEvent {
    // message fields
    event: ::protobuf::SingularPtrField<EventRecord>,
    link: ::protobuf::SingularPtrField<EventRecord>,
    commit_position: ::std::option::Option<i64>,
    prepare_position: ::std::option::Option<i64>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for ResolvedEvent {}

impl ResolvedEvent {
    pub fn new() -> ResolvedEvent {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static ResolvedEvent {
        static mut instance: ::protobuf::lazy::Lazy<ResolvedEvent> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ResolvedEvent,
        };
        unsafe {
            instance.get(ResolvedEvent::new)
        }
    }

    // required .Messages.EventRecord event = 1;

    pub fn clear_event(&mut self) {
        self.event.clear();
    }

    pub fn has_event(&self) -> bool {
        self.event.is_some()
    }

    // Param is passed by value, moved
    pub fn set_event(&mut self, v: EventRecord) {
        self.event = ::protobuf::SingularPtrField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_event(&mut self) -> &mut EventRecord {
        if self.event.is_none() {
            self.event.set_default();
        }
        self.event.as_mut().unwrap()
    }

    // Take field
    pub fn take_event(&mut self) -> EventRecord {
        self.event.take().unwrap_or_else(|| EventRecord::new())
    }

    pub fn get_event(&self) -> &EventRecord {
        self.event.as_ref().unwrap_or_else(|| EventRecord::default_instance())
    }

    fn get_event_for_reflect(&self) -> &::protobuf::SingularPtrField<EventRecord> {
        &self.event
    }

    fn mut_event_for_reflect(&mut self) -> &mut ::protobuf::SingularPtrField<EventRecord> {
        &mut self.event
    }

    // optional .Messages.EventRecord link = 2;

    pub fn clear_link(&mut self) {
        self.link.clear();
    }

    pub fn has_link(&self) -> bool {
        self.link.is_some()
    }

    // Param is passed by value, moved
    pub fn set_link(&mut self, v: EventRecord) {
        self.link = ::protobuf::SingularPtrField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_link(&mut self) -> &mut EventRecord {
        if self.link.is_none() {
            self.link.set_default();
        }
        self.link.as_mut().unwrap()
    }

    // Take field
    pub fn take_link(&mut self) -> EventRecord {
        self.link.take().unwrap_or_else(|| EventRecord::new())
    }

    pub fn get_link(&self) -> &EventRecord {
        self.link.as_ref().unwrap_or_else(|| EventRecord::default_instance())
    }

    fn get_link_for_reflect(&self) -> &::protobuf::SingularPtrField<EventRecord> {
        &self.link
    }

    fn mut_link_for_reflect(&mut self) -> &mut ::protobuf::SingularPtrField<EventRecord> {
        &mut self.link
    }

    // required int64 commit_position = 3;

    pub fn clear_commit_position(&mut self) {
        self.commit_position = ::std::option::Option::None;
    }

    pub fn has_commit_position(&self) -> bool {
        self.commit_position.is_some()
    }

    // Param is passed by value, moved
    pub fn set_commit_position(&mut self, v: i64) {
        self.commit_position = ::std::option::Option::Some(v);
    }

    pub fn get_commit_position(&self) -> i64 {
        self.commit_position.unwrap_or(0)
    }

    fn get_commit_position_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.commit_position
    }

    fn mut_commit_position_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.commit_position
    }

    // required int64 prepare_position = 4;

    pub fn clear_prepare_position(&mut self) {
        self.prepare_position = ::std::option::Option::None;
    }

    pub fn has_prepare_position(&self) -> bool {
        self.prepare_position.is_some()
    }

    // Param is passed by value, moved
    pub fn set_prepare_position(&mut self, v: i64) {
        self.prepare_position = ::std::option::Option::Some(v);
    }

    pub fn get_prepare_position(&self) -> i64 {
        self.prepare_position.unwrap_or(0)
    }

    fn get_prepare_position_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.prepare_position
    }

    fn mut_prepare_position_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.prepare_position
    }
}

impl ::protobuf::Message for ResolvedEvent {
    fn is_initialized(&self) -> bool {
        if self.event.is_none() {
            return false;
        }
        if self.commit_position.is_none() {
            return false;
        }
        if self.prepare_position.is_none() {
            return false;
        }
        for v in &self.event {
            if !v.is_initialized() {
                return false;
            }
        };
        for v in &self.link {
            if !v.is_initialized() {
                return false;
            }
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_message_into(wire_type, is, &mut self.event)?;
                },
                2 => {
                    ::protobuf::rt::read_singular_message_into(wire_type, is, &mut self.link)?;
                },
                3 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.commit_position = ::std::option::Option::Some(tmp);
                },
                4 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.prepare_position = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(ref v) = self.event.as_ref() {
            let len = v.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        }
        if let Some(ref v) = self.link.as_ref() {
            let len = v.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        }
        if let Some(v) = self.commit_position {
            my_size += ::protobuf::rt::value_size(3, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(v) = self.prepare_position {
            my_size += ::protobuf::rt::value_size(4, v, ::protobuf::wire_format::WireTypeVarint);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(ref v) = self.event.as_ref() {
            os.write_tag(1, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        }
        if let Some(ref v) = self.link.as_ref() {
            os.write_tag(2, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        }
        if let Some(v) = self.commit_position {
            os.write_int64(3, v)?;
        }
        if let Some(v) = self.prepare_position {
            os.write_int64(4, v)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for ResolvedEvent {
    fn new() -> ResolvedEvent {
        ResolvedEvent::new()
    }

    fn descriptor_static(_: ::std::option::Option<ResolvedEvent>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_singular_ptr_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<EventRecord>>(
                    "event",
                    ResolvedEvent::get_event_for_reflect,
                    ResolvedEvent::mut_event_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_ptr_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<EventRecord>>(
                    "link",
                    ResolvedEvent::get_link_for_reflect,
                    ResolvedEvent::mut_link_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "commit_position",
                    ResolvedEvent::get_commit_position_for_reflect,
                    ResolvedEvent::mut_commit_position_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "prepare_position",
                    ResolvedEvent::get_prepare_position_for_reflect,
                    ResolvedEvent::mut_prepare_position_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<ResolvedEvent>(
                    "ResolvedEvent",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for ResolvedEvent {
    fn clear(&mut self) {
        self.clear_event();
        self.clear_link();
        self.clear_commit_position();
        self.clear_prepare_position();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for ResolvedEvent {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for ResolvedEvent {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct WriteEvents {
    // message fields
    event_stream_id: ::std::option::Option<::protobuf::chars::Chars>,
    expected_version: ::std::option::Option<i64>,
    events: ::protobuf::RepeatedField<NewEvent>,
    require_master: ::std::option::Option<bool>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for WriteEvents {}

impl WriteEvents {
    pub fn new() -> WriteEvents {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static WriteEvents {
        static mut instance: ::protobuf::lazy::Lazy<WriteEvents> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const WriteEvents,
        };
        unsafe {
            instance.get(WriteEvents::new)
        }
    }

    // required string event_stream_id = 1;

    pub fn clear_event_stream_id(&mut self) {
        self.event_stream_id = ::std::option::Option::None;
    }

    pub fn has_event_stream_id(&self) -> bool {
        self.event_stream_id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_event_stream_id(&mut self, v: ::protobuf::chars::Chars) {
        self.event_stream_id = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_event_stream_id(&mut self) -> &mut ::protobuf::chars::Chars {
        if self.event_stream_id.is_none() {
            self.event_stream_id = ::std::option::Option::Some(::protobuf::chars::Chars::new());
        }
        self.event_stream_id.as_mut().unwrap()
    }

    // Take field
    pub fn take_event_stream_id(&mut self) -> ::protobuf::chars::Chars {
        self.event_stream_id.take().unwrap_or_else(|| ::protobuf::chars::Chars::new())
    }

    pub fn get_event_stream_id(&self) -> &str {
        match self.event_stream_id.as_ref() {
            Some(v) => v,
            None => "",
        }
    }

    fn get_event_stream_id_for_reflect(&self) -> &::std::option::Option<::protobuf::chars::Chars> {
        &self.event_stream_id
    }

    fn mut_event_stream_id_for_reflect(&mut self) -> &mut ::std::option::Option<::protobuf::chars::Chars> {
        &mut self.event_stream_id
    }

    // required int64 expected_version = 2;

    pub fn clear_expected_version(&mut self) {
        self.expected_version = ::std::option::Option::None;
    }

    pub fn has_expected_version(&self) -> bool {
        self.expected_version.is_some()
    }

    // Param is passed by value, moved
    pub fn set_expected_version(&mut self, v: i64) {
        self.expected_version = ::std::option::Option::Some(v);
    }

    pub fn get_expected_version(&self) -> i64 {
        self.expected_version.unwrap_or(0)
    }

    fn get_expected_version_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.expected_version
    }

    fn mut_expected_version_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.expected_version
    }

    // repeated .Messages.NewEvent events = 3;

    pub fn clear_events(&mut self) {
        self.events.clear();
    }

    // Param is passed by value, moved
    pub fn set_events(&mut self, v: ::protobuf::RepeatedField<NewEvent>) {
        self.events = v;
    }

    // Mutable pointer to the field.
    pub fn mut_events(&mut self) -> &mut ::protobuf::RepeatedField<NewEvent> {
        &mut self.events
    }

    // Take field
    pub fn take_events(&mut self) -> ::protobuf::RepeatedField<NewEvent> {
        ::std::mem::replace(&mut self.events, ::protobuf::RepeatedField::new())
    }

    pub fn get_events(&self) -> &[NewEvent] {
        &self.events
    }

    fn get_events_for_reflect(&self) -> &::protobuf::RepeatedField<NewEvent> {
        &self.events
    }

    fn mut_events_for_reflect(&mut self) -> &mut ::protobuf::RepeatedField<NewEvent> {
        &mut self.events
    }

    // required bool require_master = 4;

    pub fn clear_require_master(&mut self) {
        self.require_master = ::std::option::Option::None;
    }

    pub fn has_require_master(&self) -> bool {
        self.require_master.is_some()
    }

    // Param is passed by value, moved
    pub fn set_require_master(&mut self, v: bool) {
        self.require_master = ::std::option::Option::Some(v);
    }

    pub fn get_require_master(&self) -> bool {
        self.require_master.unwrap_or(false)
    }

    fn get_require_master_for_reflect(&self) -> &::std::option::Option<bool> {
        &self.require_master
    }

    fn mut_require_master_for_reflect(&mut self) -> &mut ::std::option::Option<bool> {
        &mut self.require_master
    }
}

impl ::protobuf::Message for WriteEvents {
    fn is_initialized(&self) -> bool {
        if self.event_stream_id.is_none() {
            return false;
        }
        if self.expected_version.is_none() {
            return false;
        }
        if self.require_master.is_none() {
            return false;
        }
        for v in &self.events {
            if !v.is_initialized() {
                return false;
            }
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_carllerche_string_into(wire_type, is, &mut self.event_stream_id)?;
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.expected_version = ::std::option::Option::Some(tmp);
                },
                3 => {
                    ::protobuf::rt::read_repeated_message_into(wire_type, is, &mut self.events)?;
                },
                4 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_bool()?;
                    self.require_master = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(ref v) = self.event_stream_id.as_ref() {
            my_size += ::protobuf::rt::string_size(1, &v);
        }
        if let Some(v) = self.expected_version {
            my_size += ::protobuf::rt::value_size(2, v, ::protobuf::wire_format::WireTypeVarint);
        }
        for value in &self.events {
            let len = value.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        };
        if let Some(v) = self.require_master {
            my_size += 2;
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(ref v) = self.event_stream_id.as_ref() {
            os.write_string(1, v)?;
        }
        if let Some(v) = self.expected_version {
            os.write_int64(2, v)?;
        }
        for v in &self.events {
            os.write_tag(3, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        };
        if let Some(v) = self.require_master {
            os.write_bool(4, v)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for WriteEvents {
    fn new() -> WriteEvents {
        WriteEvents::new()
    }

    fn descriptor_static(_: ::std::option::Option<WriteEvents>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheChars>(
                    "event_stream_id",
                    WriteEvents::get_event_stream_id_for_reflect,
                    WriteEvents::mut_event_stream_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "expected_version",
                    WriteEvents::get_expected_version_for_reflect,
                    WriteEvents::mut_expected_version_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_repeated_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<NewEvent>>(
                    "events",
                    WriteEvents::get_events_for_reflect,
                    WriteEvents::mut_events_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeBool>(
                    "require_master",
                    WriteEvents::get_require_master_for_reflect,
                    WriteEvents::mut_require_master_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<WriteEvents>(
                    "WriteEvents",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for WriteEvents {
    fn clear(&mut self) {
        self.clear_event_stream_id();
        self.clear_expected_version();
        self.clear_events();
        self.clear_require_master();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for WriteEvents {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for WriteEvents {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct WriteEventsCompleted {
    // message fields
    result: ::std::option::Option<OperationResult>,
    message: ::std::option::Option<::protobuf::chars::Chars>,
    first_event_number: ::std::option::Option<i64>,
    last_event_number: ::std::option::Option<i64>,
    prepare_position: ::std::option::Option<i64>,
    commit_position: ::std::option::Option<i64>,
    current_version: ::std::option::Option<i64>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for WriteEventsCompleted {}

impl WriteEventsCompleted {
    pub fn new() -> WriteEventsCompleted {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static WriteEventsCompleted {
        static mut instance: ::protobuf::lazy::Lazy<WriteEventsCompleted> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const WriteEventsCompleted,
        };
        unsafe {
            instance.get(WriteEventsCompleted::new)
        }
    }

    // required .Messages.OperationResult result = 1;

    pub fn clear_result(&mut self) {
        self.result = ::std::option::Option::None;
    }

    pub fn has_result(&self) -> bool {
        self.result.is_some()
    }

    // Param is passed by value, moved
    pub fn set_result(&mut self, v: OperationResult) {
        self.result = ::std::option::Option::Some(v);
    }

    pub fn get_result(&self) -> OperationResult {
        self.result.unwrap_or(OperationResult::Success)
    }

    fn get_result_for_reflect(&self) -> &::std::option::Option<OperationResult> {
        &self.result
    }

    fn mut_result_for_reflect(&mut self) -> &mut ::std::option::Option<OperationResult> {
        &mut self.result
    }

    // optional string message = 2;

    pub fn clear_message(&mut self) {
        self.message = ::std::option::Option::None;
    }

    pub fn has_message(&self) -> bool {
        self.message.is_some()
    }

    // Param is passed by value, moved
    pub fn set_message(&mut self, v: ::protobuf::chars::Chars) {
        self.message = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_message(&mut self) -> &mut ::protobuf::chars::Chars {
        if self.message.is_none() {
            self.message = ::std::option::Option::Some(::protobuf::chars::Chars::new());
        }
        self.message.as_mut().unwrap()
    }

    // Take field
    pub fn take_message(&mut self) -> ::protobuf::chars::Chars {
        self.message.take().unwrap_or_else(|| ::protobuf::chars::Chars::new())
    }

    pub fn get_message(&self) -> &str {
        match self.message.as_ref() {
            Some(v) => v,
            None => "",
        }
    }

    fn get_message_for_reflect(&self) -> &::std::option::Option<::protobuf::chars::Chars> {
        &self.message
    }

    fn mut_message_for_reflect(&mut self) -> &mut ::std::option::Option<::protobuf::chars::Chars> {
        &mut self.message
    }

    // required int64 first_event_number = 3;

    pub fn clear_first_event_number(&mut self) {
        self.first_event_number = ::std::option::Option::None;
    }

    pub fn has_first_event_number(&self) -> bool {
        self.first_event_number.is_some()
    }

    // Param is passed by value, moved
    pub fn set_first_event_number(&mut self, v: i64) {
        self.first_event_number = ::std::option::Option::Some(v);
    }

    pub fn get_first_event_number(&self) -> i64 {
        self.first_event_number.unwrap_or(0)
    }

    fn get_first_event_number_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.first_event_number
    }

    fn mut_first_event_number_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.first_event_number
    }

    // required int64 last_event_number = 4;

    pub fn clear_last_event_number(&mut self) {
        self.last_event_number = ::std::option::Option::None;
    }

    pub fn has_last_event_number(&self) -> bool {
        self.last_event_number.is_some()
    }

    // Param is passed by value, moved
    pub fn set_last_event_number(&mut self, v: i64) {
        self.last_event_number = ::std::option::Option::Some(v);
    }

    pub fn get_last_event_number(&self) -> i64 {
        self.last_event_number.unwrap_or(0)
    }

    fn get_last_event_number_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.last_event_number
    }

    fn mut_last_event_number_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.last_event_number
    }

    // optional int64 prepare_position = 5;

    pub fn clear_prepare_position(&mut self) {
        self.prepare_position = ::std::option::Option::None;
    }

    pub fn has_prepare_position(&self) -> bool {
        self.prepare_position.is_some()
    }

    // Param is passed by value, moved
    pub fn set_prepare_position(&mut self, v: i64) {
        self.prepare_position = ::std::option::Option::Some(v);
    }

    pub fn get_prepare_position(&self) -> i64 {
        self.prepare_position.unwrap_or(0)
    }

    fn get_prepare_position_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.prepare_position
    }

    fn mut_prepare_position_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.prepare_position
    }

    // optional int64 commit_position = 6;

    pub fn clear_commit_position(&mut self) {
        self.commit_position = ::std::option::Option::None;
    }

    pub fn has_commit_position(&self) -> bool {
        self.commit_position.is_some()
    }

    // Param is passed by value, moved
    pub fn set_commit_position(&mut self, v: i64) {
        self.commit_position = ::std::option::Option::Some(v);
    }

    pub fn get_commit_position(&self) -> i64 {
        self.commit_position.unwrap_or(0)
    }

    fn get_commit_position_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.commit_position
    }

    fn mut_commit_position_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.commit_position
    }

    // optional int64 current_version = 7;

    pub fn clear_current_version(&mut self) {
        self.current_version = ::std::option::Option::None;
    }

    pub fn has_current_version(&self) -> bool {
        self.current_version.is_some()
    }

    // Param is passed by value, moved
    pub fn set_current_version(&mut self, v: i64) {
        self.current_version = ::std::option::Option::Some(v);
    }

    pub fn get_current_version(&self) -> i64 {
        self.current_version.unwrap_or(0)
    }

    fn get_current_version_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.current_version
    }

    fn mut_current_version_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.current_version
    }
}

impl ::protobuf::Message for WriteEventsCompleted {
    fn is_initialized(&self) -> bool {
        if self.result.is_none() {
            return false;
        }
        if self.first_event_number.is_none() {
            return false;
        }
        if self.last_event_number.is_none() {
            return false;
        }
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_proto2_enum_with_unknown_fields_into(wire_type, is, &mut self.result, 1, &mut self.unknown_fields)?
                },
                2 => {
                    ::protobuf::rt::read_singular_carllerche_string_into(wire_type, is, &mut self.message)?;
                },
                3 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.first_event_number = ::std::option::Option::Some(tmp);
                },
                4 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.last_event_number = ::std::option::Option::Some(tmp);
                },
                5 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.prepare_position = ::std::option::Option::Some(tmp);
                },
                6 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.commit_position = ::std::option::Option::Some(tmp);
                },
                7 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.current_version = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.result {
            my_size += ::protobuf::rt::enum_size(1, v);
        }
        if let Some(ref v) = self.message.as_ref() {
            my_size += ::protobuf::rt::string_size(2, &v);
        }
        if let Some(v) = self.first_event_number {
            my_size += ::protobuf::rt::value_size(3, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(v) = self.last_event_number {
            my_size += ::protobuf::rt::value_size(4, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(v) = self.prepare_position {
            my_size += ::protobuf::rt::value_size(5, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(v) = self.commit_position {
            my_size += ::protobuf::rt::value_size(6, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(v) = self.current_version {
            my_size += ::protobuf::rt::value_size(7, v, ::protobuf::wire_format::WireTypeVarint);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.result {
            os.write_enum(1, v.value())?;
        }
        if let Some(ref v) = self.message.as_ref() {
            os.write_string(2, v)?;
        }
        if let Some(v) = self.first_event_number {
            os.write_int64(3, v)?;
        }
        if let Some(v) = self.last_event_number {
            os.write_int64(4, v)?;
        }
        if let Some(v) = self.prepare_position {
            os.write_int64(5, v)?;
        }
        if let Some(v) = self.commit_position {
            os.write_int64(6, v)?;
        }
        if let Some(v) = self.current_version {
            os.write_int64(7, v)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for WriteEventsCompleted {
    fn new() -> WriteEventsCompleted {
        WriteEventsCompleted::new()
    }

    fn descriptor_static(_: ::std::option::Option<WriteEventsCompleted>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeEnum<OperationResult>>(
                    "result",
                    WriteEventsCompleted::get_result_for_reflect,
                    WriteEventsCompleted::mut_result_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheChars>(
                    "message",
                    WriteEventsCompleted::get_message_for_reflect,
                    WriteEventsCompleted::mut_message_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "first_event_number",
                    WriteEventsCompleted::get_first_event_number_for_reflect,
                    WriteEventsCompleted::mut_first_event_number_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "last_event_number",
                    WriteEventsCompleted::get_last_event_number_for_reflect,
                    WriteEventsCompleted::mut_last_event_number_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "prepare_position",
                    WriteEventsCompleted::get_prepare_position_for_reflect,
                    WriteEventsCompleted::mut_prepare_position_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "commit_position",
                    WriteEventsCompleted::get_commit_position_for_reflect,
                    WriteEventsCompleted::mut_commit_position_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "current_version",
                    WriteEventsCompleted::get_current_version_for_reflect,
                    WriteEventsCompleted::mut_current_version_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<WriteEventsCompleted>(
                    "WriteEventsCompleted",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for WriteEventsCompleted {
    fn clear(&mut self) {
        self.clear_result();
        self.clear_message();
        self.clear_first_event_number();
        self.clear_last_event_number();
        self.clear_prepare_position();
        self.clear_commit_position();
        self.clear_current_version();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for WriteEventsCompleted {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for WriteEventsCompleted {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct ReadEvent {
    // message fields
    event_stream_id: ::std::option::Option<::protobuf::chars::Chars>,
    event_number: ::std::option::Option<i64>,
    resolve_link_tos: ::std::option::Option<bool>,
    require_master: ::std::option::Option<bool>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for ReadEvent {}

impl ReadEvent {
    pub fn new() -> ReadEvent {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static ReadEvent {
        static mut instance: ::protobuf::lazy::Lazy<ReadEvent> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ReadEvent,
        };
        unsafe {
            instance.get(ReadEvent::new)
        }
    }

    // required string event_stream_id = 1;

    pub fn clear_event_stream_id(&mut self) {
        self.event_stream_id = ::std::option::Option::None;
    }

    pub fn has_event_stream_id(&self) -> bool {
        self.event_stream_id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_event_stream_id(&mut self, v: ::protobuf::chars::Chars) {
        self.event_stream_id = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_event_stream_id(&mut self) -> &mut ::protobuf::chars::Chars {
        if self.event_stream_id.is_none() {
            self.event_stream_id = ::std::option::Option::Some(::protobuf::chars::Chars::new());
        }
        self.event_stream_id.as_mut().unwrap()
    }

    // Take field
    pub fn take_event_stream_id(&mut self) -> ::protobuf::chars::Chars {
        self.event_stream_id.take().unwrap_or_else(|| ::protobuf::chars::Chars::new())
    }

    pub fn get_event_stream_id(&self) -> &str {
        match self.event_stream_id.as_ref() {
            Some(v) => v,
            None => "",
        }
    }

    fn get_event_stream_id_for_reflect(&self) -> &::std::option::Option<::protobuf::chars::Chars> {
        &self.event_stream_id
    }

    fn mut_event_stream_id_for_reflect(&mut self) -> &mut ::std::option::Option<::protobuf::chars::Chars> {
        &mut self.event_stream_id
    }

    // required int64 event_number = 2;

    pub fn clear_event_number(&mut self) {
        self.event_number = ::std::option::Option::None;
    }

    pub fn has_event_number(&self) -> bool {
        self.event_number.is_some()
    }

    // Param is passed by value, moved
    pub fn set_event_number(&mut self, v: i64) {
        self.event_number = ::std::option::Option::Some(v);
    }

    pub fn get_event_number(&self) -> i64 {
        self.event_number.unwrap_or(0)
    }

    fn get_event_number_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.event_number
    }

    fn mut_event_number_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.event_number
    }

    // required bool resolve_link_tos = 3;

    pub fn clear_resolve_link_tos(&mut self) {
        self.resolve_link_tos = ::std::option::Option::None;
    }

    pub fn has_resolve_link_tos(&self) -> bool {
        self.resolve_link_tos.is_some()
    }

    // Param is passed by value, moved
    pub fn set_resolve_link_tos(&mut self, v: bool) {
        self.resolve_link_tos = ::std::option::Option::Some(v);
    }

    pub fn get_resolve_link_tos(&self) -> bool {
        self.resolve_link_tos.unwrap_or(false)
    }

    fn get_resolve_link_tos_for_reflect(&self) -> &::std::option::Option<bool> {
        &self.resolve_link_tos
    }

    fn mut_resolve_link_tos_for_reflect(&mut self) -> &mut ::std::option::Option<bool> {
        &mut self.resolve_link_tos
    }

    // required bool require_master = 4;

    pub fn clear_require_master(&mut self) {
        self.require_master = ::std::option::Option::None;
    }

    pub fn has_require_master(&self) -> bool {
        self.require_master.is_some()
    }

    // Param is passed by value, moved
    pub fn set_require_master(&mut self, v: bool) {
        self.require_master = ::std::option::Option::Some(v);
    }

    pub fn get_require_master(&self) -> bool {
        self.require_master.unwrap_or(false)
    }

    fn get_require_master_for_reflect(&self) -> &::std::option::Option<bool> {
        &self.require_master
    }

    fn mut_require_master_for_reflect(&mut self) -> &mut ::std::option::Option<bool> {
        &mut self.require_master
    }
}

impl ::protobuf::Message for ReadEvent {
    fn is_initialized(&self) -> bool {
        if self.event_stream_id.is_none() {
            return false;
        }
        if self.event_number.is_none() {
            return false;
        }
        if self.resolve_link_tos.is_none() {
            return false;
        }
        if self.require_master.is_none() {
            return false;
        }
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_carllerche_string_into(wire_type, is, &mut self.event_stream_id)?;
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.event_number = ::std::option::Option::Some(tmp);
                },
                3 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_bool()?;
                    self.resolve_link_tos = ::std::option::Option::Some(tmp);
                },
                4 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_bool()?;
                    self.require_master = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(ref v) = self.event_stream_id.as_ref() {
            my_size += ::protobuf::rt::string_size(1, &v);
        }
        if let Some(v) = self.event_number {
            my_size += ::protobuf::rt::value_size(2, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(v) = self.resolve_link_tos {
            my_size += 2;
        }
        if let Some(v) = self.require_master {
            my_size += 2;
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(ref v) = self.event_stream_id.as_ref() {
            os.write_string(1, v)?;
        }
        if let Some(v) = self.event_number {
            os.write_int64(2, v)?;
        }
        if let Some(v) = self.resolve_link_tos {
            os.write_bool(3, v)?;
        }
        if let Some(v) = self.require_master {
            os.write_bool(4, v)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for ReadEvent {
    fn new() -> ReadEvent {
        ReadEvent::new()
    }

    fn descriptor_static(_: ::std::option::Option<ReadEvent>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheChars>(
                    "event_stream_id",
                    ReadEvent::get_event_stream_id_for_reflect,
                    ReadEvent::mut_event_stream_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "event_number",
                    ReadEvent::get_event_number_for_reflect,
                    ReadEvent::mut_event_number_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeBool>(
                    "resolve_link_tos",
                    ReadEvent::get_resolve_link_tos_for_reflect,
                    ReadEvent::mut_resolve_link_tos_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeBool>(
                    "require_master",
                    ReadEvent::get_require_master_for_reflect,
                    ReadEvent::mut_require_master_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<ReadEvent>(
                    "ReadEvent",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for ReadEvent {
    fn clear(&mut self) {
        self.clear_event_stream_id();
        self.clear_event_number();
        self.clear_resolve_link_tos();
        self.clear_require_master();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for ReadEvent {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for ReadEvent {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct ReadEventCompleted {
    // message fields
    result: ::std::option::Option<ReadEventCompleted_ReadEventResult>,
    event: ::protobuf::SingularPtrField<ResolvedIndexedEvent>,
    error: ::std::option::Option<::protobuf::chars::Chars>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for ReadEventCompleted {}

impl ReadEventCompleted {
    pub fn new() -> ReadEventCompleted {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static ReadEventCompleted {
        static mut instance: ::protobuf::lazy::Lazy<ReadEventCompleted> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ReadEventCompleted,
        };
        unsafe {
            instance.get(ReadEventCompleted::new)
        }
    }

    // required .Messages.ReadEventCompleted.ReadEventResult result = 1;

    pub fn clear_result(&mut self) {
        self.result = ::std::option::Option::None;
    }

    pub fn has_result(&self) -> bool {
        self.result.is_some()
    }

    // Param is passed by value, moved
    pub fn set_result(&mut self, v: ReadEventCompleted_ReadEventResult) {
        self.result = ::std::option::Option::Some(v);
    }

    pub fn get_result(&self) -> ReadEventCompleted_ReadEventResult {
        self.result.unwrap_or(ReadEventCompleted_ReadEventResult::Success)
    }

    fn get_result_for_reflect(&self) -> &::std::option::Option<ReadEventCompleted_ReadEventResult> {
        &self.result
    }

    fn mut_result_for_reflect(&mut self) -> &mut ::std::option::Option<ReadEventCompleted_ReadEventResult> {
        &mut self.result
    }

    // required .Messages.ResolvedIndexedEvent event = 2;

    pub fn clear_event(&mut self) {
        self.event.clear();
    }

    pub fn has_event(&self) -> bool {
        self.event.is_some()
    }

    // Param is passed by value, moved
    pub fn set_event(&mut self, v: ResolvedIndexedEvent) {
        self.event = ::protobuf::SingularPtrField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_event(&mut self) -> &mut ResolvedIndexedEvent {
        if self.event.is_none() {
            self.event.set_default();
        }
        self.event.as_mut().unwrap()
    }

    // Take field
    pub fn take_event(&mut self) -> ResolvedIndexedEvent {
        self.event.take().unwrap_or_else(|| ResolvedIndexedEvent::new())
    }

    pub fn get_event(&self) -> &ResolvedIndexedEvent {
        self.event.as_ref().unwrap_or_else(|| ResolvedIndexedEvent::default_instance())
    }

    fn get_event_for_reflect(&self) -> &::protobuf::SingularPtrField<ResolvedIndexedEvent> {
        &self.event
    }

    fn mut_event_for_reflect(&mut self) -> &mut ::protobuf::SingularPtrField<ResolvedIndexedEvent> {
        &mut self.event
    }

    // optional string error = 3;

    pub fn clear_error(&mut self) {
        self.error = ::std::option::Option::None;
    }

    pub fn has_error(&self) -> bool {
        self.error.is_some()
    }

    // Param is passed by value, moved
    pub fn set_error(&mut self, v: ::protobuf::chars::Chars) {
        self.error = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_error(&mut self) -> &mut ::protobuf::chars::Chars {
        if self.error.is_none() {
            self.error = ::std::option::Option::Some(::protobuf::chars::Chars::new());
        }
        self.error.as_mut().unwrap()
    }

    // Take field
    pub fn take_error(&mut self) -> ::protobuf::chars::Chars {
        self.error.take().unwrap_or_else(|| ::protobuf::chars::Chars::new())
    }

    pub fn get_error(&self) -> &str {
        match self.error.as_ref() {
            Some(v) => v,
            None => "",
        }
    }

    fn get_error_for_reflect(&self) -> &::std::option::Option<::protobuf::chars::Chars> {
        &self.error
    }

    fn mut_error_for_reflect(&mut self) -> &mut ::std::option::Option<::protobuf::chars::Chars> {
        &mut self.error
    }
}

impl ::protobuf::Message for ReadEventCompleted {
    fn is_initialized(&self) -> bool {
        if self.result.is_none() {
            return false;
        }
        if self.event.is_none() {
            return false;
        }
        for v in &self.event {
            if !v.is_initialized() {
                return false;
            }
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_proto2_enum_with_unknown_fields_into(wire_type, is, &mut self.result, 1, &mut self.unknown_fields)?
                },
                2 => {
                    ::protobuf::rt::read_singular_message_into(wire_type, is, &mut self.event)?;
                },
                3 => {
                    ::protobuf::rt::read_singular_carllerche_string_into(wire_type, is, &mut self.error)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.result {
            my_size += ::protobuf::rt::enum_size(1, v);
        }
        if let Some(ref v) = self.event.as_ref() {
            let len = v.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        }
        if let Some(ref v) = self.error.as_ref() {
            my_size += ::protobuf::rt::string_size(3, &v);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.result {
            os.write_enum(1, v.value())?;
        }
        if let Some(ref v) = self.event.as_ref() {
            os.write_tag(2, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        }
        if let Some(ref v) = self.error.as_ref() {
            os.write_string(3, v)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for ReadEventCompleted {
    fn new() -> ReadEventCompleted {
        ReadEventCompleted::new()
    }

    fn descriptor_static(_: ::std::option::Option<ReadEventCompleted>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeEnum<ReadEventCompleted_ReadEventResult>>(
                    "result",
                    ReadEventCompleted::get_result_for_reflect,
                    ReadEventCompleted::mut_result_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_ptr_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<ResolvedIndexedEvent>>(
                    "event",
                    ReadEventCompleted::get_event_for_reflect,
                    ReadEventCompleted::mut_event_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheChars>(
                    "error",
                    ReadEventCompleted::get_error_for_reflect,
                    ReadEventCompleted::mut_error_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<ReadEventCompleted>(
                    "ReadEventCompleted",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for ReadEventCompleted {
    fn clear(&mut self) {
        self.clear_result();
        self.clear_event();
        self.clear_error();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for ReadEventCompleted {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for ReadEventCompleted {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(Clone,PartialEq,Eq,Debug,Hash)]
pub enum ReadEventCompleted_ReadEventResult {
    Success = 0,
    NotFound = 1,
    NoStream = 2,
    StreamDeleted = 3,
    Error = 4,
    AccessDenied = 5,
}

impl ::protobuf::ProtobufEnum for ReadEventCompleted_ReadEventResult {
    fn value(&self) -> i32 {
        *self as i32
    }

    fn from_i32(value: i32) -> ::std::option::Option<ReadEventCompleted_ReadEventResult> {
        match value {
            0 => ::std::option::Option::Some(ReadEventCompleted_ReadEventResult::Success),
            1 => ::std::option::Option::Some(ReadEventCompleted_ReadEventResult::NotFound),
            2 => ::std::option::Option::Some(ReadEventCompleted_ReadEventResult::NoStream),
            3 => ::std::option::Option::Some(ReadEventCompleted_ReadEventResult::StreamDeleted),
            4 => ::std::option::Option::Some(ReadEventCompleted_ReadEventResult::Error),
            5 => ::std::option::Option::Some(ReadEventCompleted_ReadEventResult::AccessDenied),
            _ => ::std::option::Option::None
        }
    }

    fn values() -> &'static [Self] {
        static values: &'static [ReadEventCompleted_ReadEventResult] = &[
            ReadEventCompleted_ReadEventResult::Success,
            ReadEventCompleted_ReadEventResult::NotFound,
            ReadEventCompleted_ReadEventResult::NoStream,
            ReadEventCompleted_ReadEventResult::StreamDeleted,
            ReadEventCompleted_ReadEventResult::Error,
            ReadEventCompleted_ReadEventResult::AccessDenied,
        ];
        values
    }

    fn enum_descriptor_static(_: ::std::option::Option<ReadEventCompleted_ReadEventResult>) -> &'static ::protobuf::reflect::EnumDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::EnumDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::EnumDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                ::protobuf::reflect::EnumDescriptor::new("ReadEventCompleted_ReadEventResult", file_descriptor_proto())
            })
        }
    }
}

impl ::std::marker::Copy for ReadEventCompleted_ReadEventResult {
}

impl ::protobuf::reflect::ProtobufValue for ReadEventCompleted_ReadEventResult {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Enum(self.descriptor())
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct ReadStreamEvents {
    // message fields
    event_stream_id: ::std::option::Option<::protobuf::chars::Chars>,
    from_event_number: ::std::option::Option<i64>,
    max_count: ::std::option::Option<i32>,
    resolve_link_tos: ::std::option::Option<bool>,
    require_master: ::std::option::Option<bool>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for ReadStreamEvents {}

impl ReadStreamEvents {
    pub fn new() -> ReadStreamEvents {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static ReadStreamEvents {
        static mut instance: ::protobuf::lazy::Lazy<ReadStreamEvents> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ReadStreamEvents,
        };
        unsafe {
            instance.get(ReadStreamEvents::new)
        }
    }

    // required string event_stream_id = 1;

    pub fn clear_event_stream_id(&mut self) {
        self.event_stream_id = ::std::option::Option::None;
    }

    pub fn has_event_stream_id(&self) -> bool {
        self.event_stream_id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_event_stream_id(&mut self, v: ::protobuf::chars::Chars) {
        self.event_stream_id = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_event_stream_id(&mut self) -> &mut ::protobuf::chars::Chars {
        if self.event_stream_id.is_none() {
            self.event_stream_id = ::std::option::Option::Some(::protobuf::chars::Chars::new());
        }
        self.event_stream_id.as_mut().unwrap()
    }

    // Take field
    pub fn take_event_stream_id(&mut self) -> ::protobuf::chars::Chars {
        self.event_stream_id.take().unwrap_or_else(|| ::protobuf::chars::Chars::new())
    }

    pub fn get_event_stream_id(&self) -> &str {
        match self.event_stream_id.as_ref() {
            Some(v) => v,
            None => "",
        }
    }

    fn get_event_stream_id_for_reflect(&self) -> &::std::option::Option<::protobuf::chars::Chars> {
        &self.event_stream_id
    }

    fn mut_event_stream_id_for_reflect(&mut self) -> &mut ::std::option::Option<::protobuf::chars::Chars> {
        &mut self.event_stream_id
    }

    // required int64 from_event_number = 2;

    pub fn clear_from_event_number(&mut self) {
        self.from_event_number = ::std::option::Option::None;
    }

    pub fn has_from_event_number(&self) -> bool {
        self.from_event_number.is_some()
    }

    // Param is passed by value, moved
    pub fn set_from_event_number(&mut self, v: i64) {
        self.from_event_number = ::std::option::Option::Some(v);
    }

    pub fn get_from_event_number(&self) -> i64 {
        self.from_event_number.unwrap_or(0)
    }

    fn get_from_event_number_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.from_event_number
    }

    fn mut_from_event_number_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.from_event_number
    }

    // required int32 max_count = 3;

    pub fn clear_max_count(&mut self) {
        self.max_count = ::std::option::Option::None;
    }

    pub fn has_max_count(&self) -> bool {
        self.max_count.is_some()
    }

    // Param is passed by value, moved
    pub fn set_max_count(&mut self, v: i32) {
        self.max_count = ::std::option::Option::Some(v);
    }

    pub fn get_max_count(&self) -> i32 {
        self.max_count.unwrap_or(0)
    }

    fn get_max_count_for_reflect(&self) -> &::std::option::Option<i32> {
        &self.max_count
    }

    fn mut_max_count_for_reflect(&mut self) -> &mut ::std::option::Option<i32> {
        &mut self.max_count
    }

    // required bool resolve_link_tos = 4;

    pub fn clear_resolve_link_tos(&mut self) {
        self.resolve_link_tos = ::std::option::Option::None;
    }

    pub fn has_resolve_link_tos(&self) -> bool {
        self.resolve_link_tos.is_some()
    }

    // Param is passed by value, moved
    pub fn set_resolve_link_tos(&mut self, v: bool) {
        self.resolve_link_tos = ::std::option::Option::Some(v);
    }

    pub fn get_resolve_link_tos(&self) -> bool {
        self.resolve_link_tos.unwrap_or(false)
    }

    fn get_resolve_link_tos_for_reflect(&self) -> &::std::option::Option<bool> {
        &self.resolve_link_tos
    }

    fn mut_resolve_link_tos_for_reflect(&mut self) -> &mut ::std::option::Option<bool> {
        &mut self.resolve_link_tos
    }

    // required bool require_master = 5;

    pub fn clear_require_master(&mut self) {
        self.require_master = ::std::option::Option::None;
    }

    pub fn has_require_master(&self) -> bool {
        self.require_master.is_some()
    }

    // Param is passed by value, moved
    pub fn set_require_master(&mut self, v: bool) {
        self.require_master = ::std::option::Option::Some(v);
    }

    pub fn get_require_master(&self) -> bool {
        self.require_master.unwrap_or(false)
    }

    fn get_require_master_for_reflect(&self) -> &::std::option::Option<bool> {
        &self.require_master
    }

    fn mut_require_master_for_reflect(&mut self) -> &mut ::std::option::Option<bool> {
        &mut self.require_master
    }
}

impl ::protobuf::Message for ReadStreamEvents {
    fn is_initialized(&self) -> bool {
        if self.event_stream_id.is_none() {
            return false;
        }
        if self.from_event_number.is_none() {
            return false;
        }
        if self.max_count.is_none() {
            return false;
        }
        if self.resolve_link_tos.is_none() {
            return false;
        }
        if self.require_master.is_none() {
            return false;
        }
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_carllerche_string_into(wire_type, is, &mut self.event_stream_id)?;
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.from_event_number = ::std::option::Option::Some(tmp);
                },
                3 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int32()?;
                    self.max_count = ::std::option::Option::Some(tmp);
                },
                4 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_bool()?;
                    self.resolve_link_tos = ::std::option::Option::Some(tmp);
                },
                5 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_bool()?;
                    self.require_master = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(ref v) = self.event_stream_id.as_ref() {
            my_size += ::protobuf::rt::string_size(1, &v);
        }
        if let Some(v) = self.from_event_number {
            my_size += ::protobuf::rt::value_size(2, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(v) = self.max_count {
            my_size += ::protobuf::rt::value_size(3, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(v) = self.resolve_link_tos {
            my_size += 2;
        }
        if let Some(v) = self.require_master {
            my_size += 2;
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(ref v) = self.event_stream_id.as_ref() {
            os.write_string(1, v)?;
        }
        if let Some(v) = self.from_event_number {
            os.write_int64(2, v)?;
        }
        if let Some(v) = self.max_count {
            os.write_int32(3, v)?;
        }
        if let Some(v) = self.resolve_link_tos {
            os.write_bool(4, v)?;
        }
        if let Some(v) = self.require_master {
            os.write_bool(5, v)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for ReadStreamEvents {
    fn new() -> ReadStreamEvents {
        ReadStreamEvents::new()
    }

    fn descriptor_static(_: ::std::option::Option<ReadStreamEvents>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheChars>(
                    "event_stream_id",
                    ReadStreamEvents::get_event_stream_id_for_reflect,
                    ReadStreamEvents::mut_event_stream_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "from_event_number",
                    ReadStreamEvents::get_from_event_number_for_reflect,
                    ReadStreamEvents::mut_from_event_number_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt32>(
                    "max_count",
                    ReadStreamEvents::get_max_count_for_reflect,
                    ReadStreamEvents::mut_max_count_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeBool>(
                    "resolve_link_tos",
                    ReadStreamEvents::get_resolve_link_tos_for_reflect,
                    ReadStreamEvents::mut_resolve_link_tos_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeBool>(
                    "require_master",
                    ReadStreamEvents::get_require_master_for_reflect,
                    ReadStreamEvents::mut_require_master_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<ReadStreamEvents>(
                    "ReadStreamEvents",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for ReadStreamEvents {
    fn clear(&mut self) {
        self.clear_event_stream_id();
        self.clear_from_event_number();
        self.clear_max_count();
        self.clear_resolve_link_tos();
        self.clear_require_master();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for ReadStreamEvents {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for ReadStreamEvents {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct ReadStreamEventsCompleted {
    // message fields
    events: ::protobuf::RepeatedField<ResolvedIndexedEvent>,
    result: ::std::option::Option<ReadStreamEventsCompleted_ReadStreamResult>,
    next_event_number: ::std::option::Option<i64>,
    last_event_number: ::std::option::Option<i64>,
    is_end_of_stream: ::std::option::Option<bool>,
    last_commit_position: ::std::option::Option<i64>,
    error: ::std::option::Option<::protobuf::chars::Chars>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for ReadStreamEventsCompleted {}

impl ReadStreamEventsCompleted {
    pub fn new() -> ReadStreamEventsCompleted {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static ReadStreamEventsCompleted {
        static mut instance: ::protobuf::lazy::Lazy<ReadStreamEventsCompleted> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ReadStreamEventsCompleted,
        };
        unsafe {
            instance.get(ReadStreamEventsCompleted::new)
        }
    }

    // repeated .Messages.ResolvedIndexedEvent events = 1;

    pub fn clear_events(&mut self) {
        self.events.clear();
    }

    // Param is passed by value, moved
    pub fn set_events(&mut self, v: ::protobuf::RepeatedField<ResolvedIndexedEvent>) {
        self.events = v;
    }

    // Mutable pointer to the field.
    pub fn mut_events(&mut self) -> &mut ::protobuf::RepeatedField<ResolvedIndexedEvent> {
        &mut self.events
    }

    // Take field
    pub fn take_events(&mut self) -> ::protobuf::RepeatedField<ResolvedIndexedEvent> {
        ::std::mem::replace(&mut self.events, ::protobuf::RepeatedField::new())
    }

    pub fn get_events(&self) -> &[ResolvedIndexedEvent] {
        &self.events
    }

    fn get_events_for_reflect(&self) -> &::protobuf::RepeatedField<ResolvedIndexedEvent> {
        &self.events
    }

    fn mut_events_for_reflect(&mut self) -> &mut ::protobuf::RepeatedField<ResolvedIndexedEvent> {
        &mut self.events
    }

    // required .Messages.ReadStreamEventsCompleted.ReadStreamResult result = 2;

    pub fn clear_result(&mut self) {
        self.result = ::std::option::Option::None;
    }

    pub fn has_result(&self) -> bool {
        self.result.is_some()
    }

    // Param is passed by value, moved
    pub fn set_result(&mut self, v: ReadStreamEventsCompleted_ReadStreamResult) {
        self.result = ::std::option::Option::Some(v);
    }

    pub fn get_result(&self) -> ReadStreamEventsCompleted_ReadStreamResult {
        self.result.unwrap_or(ReadStreamEventsCompleted_ReadStreamResult::Success)
    }

    fn get_result_for_reflect(&self) -> &::std::option::Option<ReadStreamEventsCompleted_ReadStreamResult> {
        &self.result
    }

    fn mut_result_for_reflect(&mut self) -> &mut ::std::option::Option<ReadStreamEventsCompleted_ReadStreamResult> {
        &mut self.result
    }

    // required int64 next_event_number = 3;

    pub fn clear_next_event_number(&mut self) {
        self.next_event_number = ::std::option::Option::None;
    }

    pub fn has_next_event_number(&self) -> bool {
        self.next_event_number.is_some()
    }

    // Param is passed by value, moved
    pub fn set_next_event_number(&mut self, v: i64) {
        self.next_event_number = ::std::option::Option::Some(v);
    }

    pub fn get_next_event_number(&self) -> i64 {
        self.next_event_number.unwrap_or(0)
    }

    fn get_next_event_number_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.next_event_number
    }

    fn mut_next_event_number_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.next_event_number
    }

    // required int64 last_event_number = 4;

    pub fn clear_last_event_number(&mut self) {
        self.last_event_number = ::std::option::Option::None;
    }

    pub fn has_last_event_number(&self) -> bool {
        self.last_event_number.is_some()
    }

    // Param is passed by value, moved
    pub fn set_last_event_number(&mut self, v: i64) {
        self.last_event_number = ::std::option::Option::Some(v);
    }

    pub fn get_last_event_number(&self) -> i64 {
        self.last_event_number.unwrap_or(0)
    }

    fn get_last_event_number_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.last_event_number
    }

    fn mut_last_event_number_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.last_event_number
    }

    // required bool is_end_of_stream = 5;

    pub fn clear_is_end_of_stream(&mut self) {
        self.is_end_of_stream = ::std::option::Option::None;
    }

    pub fn has_is_end_of_stream(&self) -> bool {
        self.is_end_of_stream.is_some()
    }

    // Param is passed by value, moved
    pub fn set_is_end_of_stream(&mut self, v: bool) {
        self.is_end_of_stream = ::std::option::Option::Some(v);
    }

    pub fn get_is_end_of_stream(&self) -> bool {
        self.is_end_of_stream.unwrap_or(false)
    }

    fn get_is_end_of_stream_for_reflect(&self) -> &::std::option::Option<bool> {
        &self.is_end_of_stream
    }

    fn mut_is_end_of_stream_for_reflect(&mut self) -> &mut ::std::option::Option<bool> {
        &mut self.is_end_of_stream
    }

    // required int64 last_commit_position = 6;

    pub fn clear_last_commit_position(&mut self) {
        self.last_commit_position = ::std::option::Option::None;
    }

    pub fn has_last_commit_position(&self) -> bool {
        self.last_commit_position.is_some()
    }

    // Param is passed by value, moved
    pub fn set_last_commit_position(&mut self, v: i64) {
        self.last_commit_position = ::std::option::Option::Some(v);
    }

    pub fn get_last_commit_position(&self) -> i64 {
        self.last_commit_position.unwrap_or(0)
    }

    fn get_last_commit_position_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.last_commit_position
    }

    fn mut_last_commit_position_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.last_commit_position
    }

    // optional string error = 7;

    pub fn clear_error(&mut self) {
        self.error = ::std::option::Option::None;
    }

    pub fn has_error(&self) -> bool {
        self.error.is_some()
    }

    // Param is passed by value, moved
    pub fn set_error(&mut self, v: ::protobuf::chars::Chars) {
        self.error = ::std::option::Option::Some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_error(&mut self) -> &mut ::protobuf::chars::Chars {
        if self.error.is_none() {
            self.error = ::std::option::Option::Some(::protobuf::chars::Chars::new());
        }
        self.error.as_mut().unwrap()
    }

    // Take field
    pub fn take_error(&mut self) -> ::protobuf::chars::Chars {
        self.error.take().unwrap_or_else(|| ::protobuf::chars::Chars::new())
    }

    pub fn get_error(&self) -> &str {
        match self.error.as_ref() {
            Some(v) => v,
            None => "",
        }
    }

    fn get_error_for_reflect(&self) -> &::std::option::Option<::protobuf::chars::Chars> {
        &self.error
    }

    fn mut_error_for_reflect(&mut self) -> &mut ::std::option::Option<::protobuf::chars::Chars> {
        &mut self.error
    }
}

impl ::protobuf::Message for ReadStreamEventsCompleted {
    fn is_initialized(&self) -> bool {
        if self.result.is_none() {
            return false;
        }
        if self.next_event_number.is_none() {
            return false;
        }
        if self.last_event_number.is_none() {
            return false;
        }
        if self.is_end_of_stream.is_none() {
            return false;
        }
        if self.last_commit_position.is_none() {
            return false;
        }
        for v in &self.events {
            if !v.is_initialized() {
                return false;
            }
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_repeated_message_into(wire_type, is, &mut self.events)?;
                },
                2 => {
                    ::protobuf::rt::read_proto2_enum_with_unknown_fields_into(wire_type, is, &mut self.result, 2, &mut self.unknown_fields)?
                },
                3 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.next_event_number = ::std::option::Option::Some(tmp);
                },
                4 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.last_event_number = ::std::option::Option::Some(tmp);
                },
                5 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_bool()?;
                    self.is_end_of_stream = ::std::option::Option::Some(tmp);
                },
                6 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.last_commit_position = ::std::option::Option::Some(tmp);
                },
                7 => {
                    ::protobuf::rt::read_singular_carllerche_string_into(wire_type, is, &mut self.error)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        for value in &self.events {
            let len = value.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        };
        if let Some(v) = self.result {
            my_size += ::protobuf::rt::enum_size(2, v);
        }
        if let Some(v) = self.next_event_number {
            my_size += ::protobuf::rt::value_size(3, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(v) = self.last_event_number {
            my_size += ::protobuf::rt::value_size(4, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(v) = self.is_end_of_stream {
            my_size += 2;
        }
        if let Some(v) = self.last_commit_position {
            my_size += ::protobuf::rt::value_size(6, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(ref v) = self.error.as_ref() {
            my_size += ::protobuf::rt::string_size(7, &v);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        for v in &self.events {
            os.write_tag(1, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        };
        if let Some(v) = self.result {
            os.write_enum(2, v.value())?;
        }
        if let Some(v) = self.next_event_number {
            os.write_int64(3, v)?;
        }
        if let Some(v) = self.last_event_number {
            os.write_int64(4, v)?;
        }
        if let Some(v) = self.is_end_of_stream {
            os.write_bool(5, v)?;
        }
        if let Some(v) = self.last_commit_position {
            os.write_int64(6, v)?;
        }
        if let Some(ref v) = self.error.as_ref() {
            os.write_string(7, v)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for ReadStreamEventsCompleted {
    fn new() -> ReadStreamEventsCompleted {
        ReadStreamEventsCompleted::new()
    }

    fn descriptor_static(_: ::std::option::Option<ReadStreamEventsCompleted>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_repeated_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<ResolvedIndexedEvent>>(
                    "events",
                    ReadStreamEventsCompleted::get_events_for_reflect,
                    ReadStreamEventsCompleted::mut_events_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeEnum<ReadStreamEventsCompleted_ReadStreamResult>>(
                    "result",
                    ReadStreamEventsCompleted::get_result_for_reflect,
                    ReadStreamEventsCompleted::mut_result_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "next_event_number",
                    ReadStreamEventsCompleted::get_next_event_number_for_reflect,
                    ReadStreamEventsCompleted::mut_next_event_number_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "last_event_number",
                    ReadStreamEventsCompleted::get_last_event_number_for_reflect,
                    ReadStreamEventsCompleted::mut_last_event_number_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeBool>(
                    "is_end_of_stream",
                    ReadStreamEventsCompleted::get_is_end_of_stream_for_reflect,
                    ReadStreamEventsCompleted::mut_is_end_of_stream_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "last_commit_position",
                    ReadStreamEventsCompleted::get_last_commit_position_for_reflect,
                    ReadStreamEventsCompleted::mut_last_commit_position_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeCarllercheChars>(
                    "error",
                    ReadStreamEventsCompleted::get_error_for_reflect,
                    ReadStreamEventsCompleted::mut_error_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<ReadStreamEventsCompleted>(
                    "ReadStreamEventsCompleted",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for ReadStreamEventsCompleted {
    fn clear(&mut self) {
        self.clear_events();
        self.clear_result();
        self.clear_next_event_number();
        self.clear_last_event_number();
        self.clear_is_end_of_stream();
        self.clear_last_commit_position();
        self.clear_error();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for ReadStreamEventsCompleted {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for ReadStreamEventsCompleted {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(Clone,PartialEq,Eq,Debug,Hash)]
pub enum ReadStreamEventsCompleted_ReadStreamResult {
    Success = 0,
    NoStream = 1,
    StreamDeleted = 2,
    NotModified = 3,
    Error = 4,
    AccessDenied = 5,
}

impl ::protobuf::ProtobufEnum for ReadStreamEventsCompleted_ReadStreamResult {
    fn value(&self) -> i32 {
        *self as i32
    }

    fn from_i32(value: i32) -> ::std::option::Option<ReadStreamEventsCompleted_ReadStreamResult> {
        match value {
            0 => ::std::option::Option::Some(ReadStreamEventsCompleted_ReadStreamResult::Success),
            1 => ::std::option::Option::Some(ReadStreamEventsCompleted_ReadStreamResult::NoStream),
            2 => ::std::option::Option::Some(ReadStreamEventsCompleted_ReadStreamResult::StreamDeleted),
            3 => ::std::option::Option::Some(ReadStreamEventsCompleted_ReadStreamResult::NotModified),
            4 => ::std::option::Option::Some(ReadStreamEventsCompleted_ReadStreamResult::Error),
            5 => ::std::option::Option::Some(ReadStreamEventsCompleted_ReadStreamResult::AccessDenied),
            _ => ::std::option::Option::None
        }
    }

    fn values() -> &'static [Self] {
        static values: &'static [ReadStreamEventsCompleted_ReadStreamResult] = &[
            ReadStreamEventsCompleted_ReadStreamResult::Success,
            ReadStreamEventsCompleted_ReadStreamResult::NoStream,
            ReadStreamEventsCompleted_ReadStreamResult::StreamDeleted,
            ReadStreamEventsCompleted_ReadStreamResult::NotModified,
            ReadStreamEventsCompleted_ReadStreamResult::Error,
            ReadStreamEventsCompleted_ReadStreamResult::AccessDenied,
        ];
        values
    }

    fn enum_descriptor_static(_: ::std::option::Option<ReadStreamEventsCompleted_ReadStreamResult>) -> &'static ::protobuf::reflect::EnumDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::EnumDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::EnumDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                ::protobuf::reflect::EnumDescriptor::new("ReadStreamEventsCompleted_ReadStreamResult", file_descriptor_proto())
            })
        }
    }
}

impl ::std::marker::Copy for ReadStreamEventsCompleted_ReadStreamResult {
}

impl ::protobuf::reflect::ProtobufValue for ReadStreamEventsCompleted_ReadStreamResult {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Enum(self.descriptor())
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct NotHandled {
    // message fields
    reason: ::std::option::Option<NotHandled_NotHandledReason>,
    additional_info: ::protobuf::SingularField<::std::vec::Vec<u8>>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for NotHandled {}

impl NotHandled {
    pub fn new() -> NotHandled {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static NotHandled {
        static mut instance: ::protobuf::lazy::Lazy<NotHandled> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const NotHandled,
        };
        unsafe {
            instance.get(NotHandled::new)
        }
    }

    // required .Messages.NotHandled.NotHandledReason reason = 1;

    pub fn clear_reason(&mut self) {
        self.reason = ::std::option::Option::None;
    }

    pub fn has_reason(&self) -> bool {
        self.reason.is_some()
    }

    // Param is passed by value, moved
    pub fn set_reason(&mut self, v: NotHandled_NotHandledReason) {
        self.reason = ::std::option::Option::Some(v);
    }

    pub fn get_reason(&self) -> NotHandled_NotHandledReason {
        self.reason.unwrap_or(NotHandled_NotHandledReason::NotReady)
    }

    fn get_reason_for_reflect(&self) -> &::std::option::Option<NotHandled_NotHandledReason> {
        &self.reason
    }

    fn mut_reason_for_reflect(&mut self) -> &mut ::std::option::Option<NotHandled_NotHandledReason> {
        &mut self.reason
    }

    // optional bytes additional_info = 2;

    pub fn clear_additional_info(&mut self) {
        self.additional_info.clear();
    }

    pub fn has_additional_info(&self) -> bool {
        self.additional_info.is_some()
    }

    // Param is passed by value, moved
    pub fn set_additional_info(&mut self, v: ::std::vec::Vec<u8>) {
        self.additional_info = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_additional_info(&mut self) -> &mut ::std::vec::Vec<u8> {
        if self.additional_info.is_none() {
            self.additional_info.set_default();
        }
        self.additional_info.as_mut().unwrap()
    }

    // Take field
    pub fn take_additional_info(&mut self) -> ::std::vec::Vec<u8> {
        self.additional_info.take().unwrap_or_else(|| ::std::vec::Vec::new())
    }

    pub fn get_additional_info(&self) -> &[u8] {
        match self.additional_info.as_ref() {
            Some(v) => &v,
            None => &[],
        }
    }

    fn get_additional_info_for_reflect(&self) -> &::protobuf::SingularField<::std::vec::Vec<u8>> {
        &self.additional_info
    }

    fn mut_additional_info_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::vec::Vec<u8>> {
        &mut self.additional_info
    }
}

impl ::protobuf::Message for NotHandled {
    fn is_initialized(&self) -> bool {
        if self.reason.is_none() {
            return false;
        }
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_proto2_enum_with_unknown_fields_into(wire_type, is, &mut self.reason, 1, &mut self.unknown_fields)?
                },
                2 => {
                    ::protobuf::rt::read_singular_bytes_into(wire_type, is, &mut self.additional_info)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.reason {
            my_size += ::protobuf::rt::enum_size(1, v);
        }
        if let Some(ref v) = self.additional_info.as_ref() {
            my_size += ::protobuf::rt::bytes_size(2, &v);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.reason {
            os.write_enum(1, v.value())?;
        }
        if let Some(ref v) = self.additional_info.as_ref() {
            os.write_bytes(2, &v)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for NotHandled {
    fn new() -> NotHandled {
        NotHandled::new()
    }

    fn descriptor_static(_: ::std::option::Option<NotHandled>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeEnum<NotHandled_NotHandledReason>>(
                    "reason",
                    NotHandled::get_reason_for_reflect,
                    NotHandled::mut_reason_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeBytes>(
                    "additional_info",
                    NotHandled::get_additional_info_for_reflect,
                    NotHandled::mut_additional_info_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<NotHandled>(
                    "NotHandled",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for NotHandled {
    fn clear(&mut self) {
        self.clear_reason();
        self.clear_additional_info();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for NotHandled {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for NotHandled {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct NotHandled_MasterInfo {
    // message fields
    external_tcp_address: ::protobuf::SingularField<::std::string::String>,
    external_tcp_port: ::std::option::Option<i32>,
    external_http_address: ::protobuf::SingularField<::std::string::String>,
    external_http_port: ::std::option::Option<i32>,
    external_secure_tcp_address: ::protobuf::SingularField<::std::string::String>,
    external_secure_tcp_port: ::std::option::Option<i32>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for NotHandled_MasterInfo {}

impl NotHandled_MasterInfo {
    pub fn new() -> NotHandled_MasterInfo {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static NotHandled_MasterInfo {
        static mut instance: ::protobuf::lazy::Lazy<NotHandled_MasterInfo> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const NotHandled_MasterInfo,
        };
        unsafe {
            instance.get(NotHandled_MasterInfo::new)
        }
    }

    // required string external_tcp_address = 1;

    pub fn clear_external_tcp_address(&mut self) {
        self.external_tcp_address.clear();
    }

    pub fn has_external_tcp_address(&self) -> bool {
        self.external_tcp_address.is_some()
    }

    // Param is passed by value, moved
    pub fn set_external_tcp_address(&mut self, v: ::std::string::String) {
        self.external_tcp_address = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_external_tcp_address(&mut self) -> &mut ::std::string::String {
        if self.external_tcp_address.is_none() {
            self.external_tcp_address.set_default();
        }
        self.external_tcp_address.as_mut().unwrap()
    }

    // Take field
    pub fn take_external_tcp_address(&mut self) -> ::std::string::String {
        self.external_tcp_address.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_external_tcp_address(&self) -> &str {
        match self.external_tcp_address.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_external_tcp_address_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.external_tcp_address
    }

    fn mut_external_tcp_address_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.external_tcp_address
    }

    // required int32 external_tcp_port = 2;

    pub fn clear_external_tcp_port(&mut self) {
        self.external_tcp_port = ::std::option::Option::None;
    }

    pub fn has_external_tcp_port(&self) -> bool {
        self.external_tcp_port.is_some()
    }

    // Param is passed by value, moved
    pub fn set_external_tcp_port(&mut self, v: i32) {
        self.external_tcp_port = ::std::option::Option::Some(v);
    }

    pub fn get_external_tcp_port(&self) -> i32 {
        self.external_tcp_port.unwrap_or(0)
    }

    fn get_external_tcp_port_for_reflect(&self) -> &::std::option::Option<i32> {
        &self.external_tcp_port
    }

    fn mut_external_tcp_port_for_reflect(&mut self) -> &mut ::std::option::Option<i32> {
        &mut self.external_tcp_port
    }

    // required string external_http_address = 3;

    pub fn clear_external_http_address(&mut self) {
        self.external_http_address.clear();
    }

    pub fn has_external_http_address(&self) -> bool {
        self.external_http_address.is_some()
    }

    // Param is passed by value, moved
    pub fn set_external_http_address(&mut self, v: ::std::string::String) {
        self.external_http_address = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_external_http_address(&mut self) -> &mut ::std::string::String {
        if self.external_http_address.is_none() {
            self.external_http_address.set_default();
        }
        self.external_http_address.as_mut().unwrap()
    }

    // Take field
    pub fn take_external_http_address(&mut self) -> ::std::string::String {
        self.external_http_address.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_external_http_address(&self) -> &str {
        match self.external_http_address.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_external_http_address_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.external_http_address
    }

    fn mut_external_http_address_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.external_http_address
    }

    // required int32 external_http_port = 4;

    pub fn clear_external_http_port(&mut self) {
        self.external_http_port = ::std::option::Option::None;
    }

    pub fn has_external_http_port(&self) -> bool {
        self.external_http_port.is_some()
    }

    // Param is passed by value, moved
    pub fn set_external_http_port(&mut self, v: i32) {
        self.external_http_port = ::std::option::Option::Some(v);
    }

    pub fn get_external_http_port(&self) -> i32 {
        self.external_http_port.unwrap_or(0)
    }

    fn get_external_http_port_for_reflect(&self) -> &::std::option::Option<i32> {
        &self.external_http_port
    }

    fn mut_external_http_port_for_reflect(&mut self) -> &mut ::std::option::Option<i32> {
        &mut self.external_http_port
    }

    // optional string external_secure_tcp_address = 5;

    pub fn clear_external_secure_tcp_address(&mut self) {
        self.external_secure_tcp_address.clear();
    }

    pub fn has_external_secure_tcp_address(&self) -> bool {
        self.external_secure_tcp_address.is_some()
    }

    // Param is passed by value, moved
    pub fn set_external_secure_tcp_address(&mut self, v: ::std::string::String) {
        self.external_secure_tcp_address = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_external_secure_tcp_address(&mut self) -> &mut ::std::string::String {
        if self.external_secure_tcp_address.is_none() {
            self.external_secure_tcp_address.set_default();
        }
        self.external_secure_tcp_address.as_mut().unwrap()
    }

    // Take field
    pub fn take_external_secure_tcp_address(&mut self) -> ::std::string::String {
        self.external_secure_tcp_address.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_external_secure_tcp_address(&self) -> &str {
        match self.external_secure_tcp_address.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_external_secure_tcp_address_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.external_secure_tcp_address
    }

    fn mut_external_secure_tcp_address_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.external_secure_tcp_address
    }

    // optional int32 external_secure_tcp_port = 6;

    pub fn clear_external_secure_tcp_port(&mut self) {
        self.external_secure_tcp_port = ::std::option::Option::None;
    }

    pub fn has_external_secure_tcp_port(&self) -> bool {
        self.external_secure_tcp_port.is_some()
    }

    // Param is passed by value, moved
    pub fn set_external_secure_tcp_port(&mut self, v: i32) {
        self.external_secure_tcp_port = ::std::option::Option::Some(v);
    }

    pub fn get_external_secure_tcp_port(&self) -> i32 {
        self.external_secure_tcp_port.unwrap_or(0)
    }

    fn get_external_secure_tcp_port_for_reflect(&self) -> &::std::option::Option<i32> {
        &self.external_secure_tcp_port
    }

    fn mut_external_secure_tcp_port_for_reflect(&mut self) -> &mut ::std::option::Option<i32> {
        &mut self.external_secure_tcp_port
    }
}

impl ::protobuf::Message for NotHandled_MasterInfo {
    fn is_initialized(&self) -> bool {
        if self.external_tcp_address.is_none() {
            return false;
        }
        if self.external_tcp_port.is_none() {
            return false;
        }
        if self.external_http_address.is_none() {
            return false;
        }
        if self.external_http_port.is_none() {
            return false;
        }
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.external_tcp_address)?;
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int32()?;
                    self.external_tcp_port = ::std::option::Option::Some(tmp);
                },
                3 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.external_http_address)?;
                },
                4 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int32()?;
                    self.external_http_port = ::std::option::Option::Some(tmp);
                },
                5 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.external_secure_tcp_address)?;
                },
                6 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int32()?;
                    self.external_secure_tcp_port = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(ref v) = self.external_tcp_address.as_ref() {
            my_size += ::protobuf::rt::string_size(1, &v);
        }
        if let Some(v) = self.external_tcp_port {
            my_size += ::protobuf::rt::value_size(2, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(ref v) = self.external_http_address.as_ref() {
            my_size += ::protobuf::rt::string_size(3, &v);
        }
        if let Some(v) = self.external_http_port {
            my_size += ::protobuf::rt::value_size(4, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(ref v) = self.external_secure_tcp_address.as_ref() {
            my_size += ::protobuf::rt::string_size(5, &v);
        }
        if let Some(v) = self.external_secure_tcp_port {
            my_size += ::protobuf::rt::value_size(6, v, ::protobuf::wire_format::WireTypeVarint);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(ref v) = self.external_tcp_address.as_ref() {
            os.write_string(1, &v)?;
        }
        if let Some(v) = self.external_tcp_port {
            os.write_int32(2, v)?;
        }
        if let Some(ref v) = self.external_http_address.as_ref() {
            os.write_string(3, &v)?;
        }
        if let Some(v) = self.external_http_port {
            os.write_int32(4, v)?;
        }
        if let Some(ref v) = self.external_secure_tcp_address.as_ref() {
            os.write_string(5, &v)?;
        }
        if let Some(v) = self.external_secure_tcp_port {
            os.write_int32(6, v)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for NotHandled_MasterInfo {
    fn new() -> NotHandled_MasterInfo {
        NotHandled_MasterInfo::new()
    }

    fn descriptor_static(_: ::std::option::Option<NotHandled_MasterInfo>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "external_tcp_address",
                    NotHandled_MasterInfo::get_external_tcp_address_for_reflect,
                    NotHandled_MasterInfo::mut_external_tcp_address_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt32>(
                    "external_tcp_port",
                    NotHandled_MasterInfo::get_external_tcp_port_for_reflect,
                    NotHandled_MasterInfo::mut_external_tcp_port_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "external_http_address",
                    NotHandled_MasterInfo::get_external_http_address_for_reflect,
                    NotHandled_MasterInfo::mut_external_http_address_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt32>(
                    "external_http_port",
                    NotHandled_MasterInfo::get_external_http_port_for_reflect,
                    NotHandled_MasterInfo::mut_external_http_port_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "external_secure_tcp_address",
                    NotHandled_MasterInfo::get_external_secure_tcp_address_for_reflect,
                    NotHandled_MasterInfo::mut_external_secure_tcp_address_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt32>(
                    "external_secure_tcp_port",
                    NotHandled_MasterInfo::get_external_secure_tcp_port_for_reflect,
                    NotHandled_MasterInfo::mut_external_secure_tcp_port_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<NotHandled_MasterInfo>(
                    "NotHandled_MasterInfo",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for NotHandled_MasterInfo {
    fn clear(&mut self) {
        self.clear_external_tcp_address();
        self.clear_external_tcp_port();
        self.clear_external_http_address();
        self.clear_external_http_port();
        self.clear_external_secure_tcp_address();
        self.clear_external_secure_tcp_port();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for NotHandled_MasterInfo {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for NotHandled_MasterInfo {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(Clone,PartialEq,Eq,Debug,Hash)]
pub enum NotHandled_NotHandledReason {
    NotReady = 0,
    TooBusy = 1,
    NotMaster = 2,
}

impl ::protobuf::ProtobufEnum for NotHandled_NotHandledReason {
    fn value(&self) -> i32 {
        *self as i32
    }

    fn from_i32(value: i32) -> ::std::option::Option<NotHandled_NotHandledReason> {
        match value {
            0 => ::std::option::Option::Some(NotHandled_NotHandledReason::NotReady),
            1 => ::std::option::Option::Some(NotHandled_NotHandledReason::TooBusy),
            2 => ::std::option::Option::Some(NotHandled_NotHandledReason::NotMaster),
            _ => ::std::option::Option::None
        }
    }

    fn values() -> &'static [Self] {
        static values: &'static [NotHandled_NotHandledReason] = &[
            NotHandled_NotHandledReason::NotReady,
            NotHandled_NotHandledReason::TooBusy,
            NotHandled_NotHandledReason::NotMaster,
        ];
        values
    }

    fn enum_descriptor_static(_: ::std::option::Option<NotHandled_NotHandledReason>) -> &'static ::protobuf::reflect::EnumDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::EnumDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::EnumDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                ::protobuf::reflect::EnumDescriptor::new("NotHandled_NotHandledReason", file_descriptor_proto())
            })
        }
    }
}

impl ::std::marker::Copy for NotHandled_NotHandledReason {
}

impl ::protobuf::reflect::ProtobufValue for NotHandled_NotHandledReason {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Enum(self.descriptor())
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct ScavengeDatabase {
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for ScavengeDatabase {}

impl ScavengeDatabase {
    pub fn new() -> ScavengeDatabase {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static ScavengeDatabase {
        static mut instance: ::protobuf::lazy::Lazy<ScavengeDatabase> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ScavengeDatabase,
        };
        unsafe {
            instance.get(ScavengeDatabase::new)
        }
    }
}

impl ::protobuf::Message for ScavengeDatabase {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for ScavengeDatabase {
    fn new() -> ScavengeDatabase {
        ScavengeDatabase::new()
    }

    fn descriptor_static(_: ::std::option::Option<ScavengeDatabase>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let fields = ::std::vec::Vec::new();
                ::protobuf::reflect::MessageDescriptor::new::<ScavengeDatabase>(
                    "ScavengeDatabase",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for ScavengeDatabase {
    fn clear(&mut self) {
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for ScavengeDatabase {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for ScavengeDatabase {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct ScavengeDatabaseCompleted {
    // message fields
    result: ::std::option::Option<ScavengeDatabaseCompleted_ScavengeResult>,
    error: ::protobuf::SingularField<::std::string::String>,
    total_time_ms: ::std::option::Option<i32>,
    total_space_saved: ::std::option::Option<i64>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for ScavengeDatabaseCompleted {}

impl ScavengeDatabaseCompleted {
    pub fn new() -> ScavengeDatabaseCompleted {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static ScavengeDatabaseCompleted {
        static mut instance: ::protobuf::lazy::Lazy<ScavengeDatabaseCompleted> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ScavengeDatabaseCompleted,
        };
        unsafe {
            instance.get(ScavengeDatabaseCompleted::new)
        }
    }

    // required .Messages.ScavengeDatabaseCompleted.ScavengeResult result = 1;

    pub fn clear_result(&mut self) {
        self.result = ::std::option::Option::None;
    }

    pub fn has_result(&self) -> bool {
        self.result.is_some()
    }

    // Param is passed by value, moved
    pub fn set_result(&mut self, v: ScavengeDatabaseCompleted_ScavengeResult) {
        self.result = ::std::option::Option::Some(v);
    }

    pub fn get_result(&self) -> ScavengeDatabaseCompleted_ScavengeResult {
        self.result.unwrap_or(ScavengeDatabaseCompleted_ScavengeResult::Success)
    }

    fn get_result_for_reflect(&self) -> &::std::option::Option<ScavengeDatabaseCompleted_ScavengeResult> {
        &self.result
    }

    fn mut_result_for_reflect(&mut self) -> &mut ::std::option::Option<ScavengeDatabaseCompleted_ScavengeResult> {
        &mut self.result
    }

    // optional string error = 2;

    pub fn clear_error(&mut self) {
        self.error.clear();
    }

    pub fn has_error(&self) -> bool {
        self.error.is_some()
    }

    // Param is passed by value, moved
    pub fn set_error(&mut self, v: ::std::string::String) {
        self.error = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_error(&mut self) -> &mut ::std::string::String {
        if self.error.is_none() {
            self.error.set_default();
        }
        self.error.as_mut().unwrap()
    }

    // Take field
    pub fn take_error(&mut self) -> ::std::string::String {
        self.error.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_error(&self) -> &str {
        match self.error.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_error_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.error
    }

    fn mut_error_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.error
    }

    // required int32 total_time_ms = 3;

    pub fn clear_total_time_ms(&mut self) {
        self.total_time_ms = ::std::option::Option::None;
    }

    pub fn has_total_time_ms(&self) -> bool {
        self.total_time_ms.is_some()
    }

    // Param is passed by value, moved
    pub fn set_total_time_ms(&mut self, v: i32) {
        self.total_time_ms = ::std::option::Option::Some(v);
    }

    pub fn get_total_time_ms(&self) -> i32 {
        self.total_time_ms.unwrap_or(0)
    }

    fn get_total_time_ms_for_reflect(&self) -> &::std::option::Option<i32> {
        &self.total_time_ms
    }

    fn mut_total_time_ms_for_reflect(&mut self) -> &mut ::std::option::Option<i32> {
        &mut self.total_time_ms
    }

    // required int64 total_space_saved = 4;

    pub fn clear_total_space_saved(&mut self) {
        self.total_space_saved = ::std::option::Option::None;
    }

    pub fn has_total_space_saved(&self) -> bool {
        self.total_space_saved.is_some()
    }

    // Param is passed by value, moved
    pub fn set_total_space_saved(&mut self, v: i64) {
        self.total_space_saved = ::std::option::Option::Some(v);
    }

    pub fn get_total_space_saved(&self) -> i64 {
        self.total_space_saved.unwrap_or(0)
    }

    fn get_total_space_saved_for_reflect(&self) -> &::std::option::Option<i64> {
        &self.total_space_saved
    }

    fn mut_total_space_saved_for_reflect(&mut self) -> &mut ::std::option::Option<i64> {
        &mut self.total_space_saved
    }
}

impl ::protobuf::Message for ScavengeDatabaseCompleted {
    fn is_initialized(&self) -> bool {
        if self.result.is_none() {
            return false;
        }
        if self.total_time_ms.is_none() {
            return false;
        }
        if self.total_space_saved.is_none() {
            return false;
        }
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_proto2_enum_with_unknown_fields_into(wire_type, is, &mut self.result, 1, &mut self.unknown_fields)?
                },
                2 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.error)?;
                },
                3 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int32()?;
                    self.total_time_ms = ::std::option::Option::Some(tmp);
                },
                4 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int64()?;
                    self.total_space_saved = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.result {
            my_size += ::protobuf::rt::enum_size(1, v);
        }
        if let Some(ref v) = self.error.as_ref() {
            my_size += ::protobuf::rt::string_size(2, &v);
        }
        if let Some(v) = self.total_time_ms {
            my_size += ::protobuf::rt::value_size(3, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(v) = self.total_space_saved {
            my_size += ::protobuf::rt::value_size(4, v, ::protobuf::wire_format::WireTypeVarint);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.result {
            os.write_enum(1, v.value())?;
        }
        if let Some(ref v) = self.error.as_ref() {
            os.write_string(2, &v)?;
        }
        if let Some(v) = self.total_time_ms {
            os.write_int32(3, v)?;
        }
        if let Some(v) = self.total_space_saved {
            os.write_int64(4, v)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for ScavengeDatabaseCompleted {
    fn new() -> ScavengeDatabaseCompleted {
        ScavengeDatabaseCompleted::new()
    }

    fn descriptor_static(_: ::std::option::Option<ScavengeDatabaseCompleted>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeEnum<ScavengeDatabaseCompleted_ScavengeResult>>(
                    "result",
                    ScavengeDatabaseCompleted::get_result_for_reflect,
                    ScavengeDatabaseCompleted::mut_result_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "error",
                    ScavengeDatabaseCompleted::get_error_for_reflect,
                    ScavengeDatabaseCompleted::mut_error_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt32>(
                    "total_time_ms",
                    ScavengeDatabaseCompleted::get_total_time_ms_for_reflect,
                    ScavengeDatabaseCompleted::mut_total_time_ms_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt64>(
                    "total_space_saved",
                    ScavengeDatabaseCompleted::get_total_space_saved_for_reflect,
                    ScavengeDatabaseCompleted::mut_total_space_saved_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<ScavengeDatabaseCompleted>(
                    "ScavengeDatabaseCompleted",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for ScavengeDatabaseCompleted {
    fn clear(&mut self) {
        self.clear_result();
        self.clear_error();
        self.clear_total_time_ms();
        self.clear_total_space_saved();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for ScavengeDatabaseCompleted {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for ScavengeDatabaseCompleted {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(Clone,PartialEq,Eq,Debug,Hash)]
pub enum ScavengeDatabaseCompleted_ScavengeResult {
    Success = 0,
    InProgress = 1,
    Failed = 2,
}

impl ::protobuf::ProtobufEnum for ScavengeDatabaseCompleted_ScavengeResult {
    fn value(&self) -> i32 {
        *self as i32
    }

    fn from_i32(value: i32) -> ::std::option::Option<ScavengeDatabaseCompleted_ScavengeResult> {
        match value {
            0 => ::std::option::Option::Some(ScavengeDatabaseCompleted_ScavengeResult::Success),
            1 => ::std::option::Option::Some(ScavengeDatabaseCompleted_ScavengeResult::InProgress),
            2 => ::std::option::Option::Some(ScavengeDatabaseCompleted_ScavengeResult::Failed),
            _ => ::std::option::Option::None
        }
    }

    fn values() -> &'static [Self] {
        static values: &'static [ScavengeDatabaseCompleted_ScavengeResult] = &[
            ScavengeDatabaseCompleted_ScavengeResult::Success,
            ScavengeDatabaseCompleted_ScavengeResult::InProgress,
            ScavengeDatabaseCompleted_ScavengeResult::Failed,
        ];
        values
    }

    fn enum_descriptor_static(_: ::std::option::Option<ScavengeDatabaseCompleted_ScavengeResult>) -> &'static ::protobuf::reflect::EnumDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::EnumDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::EnumDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                ::protobuf::reflect::EnumDescriptor::new("ScavengeDatabaseCompleted_ScavengeResult", file_descriptor_proto())
            })
        }
    }
}

impl ::std::marker::Copy for ScavengeDatabaseCompleted_ScavengeResult {
}

impl ::protobuf::reflect::ProtobufValue for ScavengeDatabaseCompleted_ScavengeResult {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Enum(self.descriptor())
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct IdentifyClient {
    // message fields
    version: ::std::option::Option<i32>,
    connection_name: ::protobuf::SingularField<::std::string::String>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for IdentifyClient {}

impl IdentifyClient {
    pub fn new() -> IdentifyClient {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static IdentifyClient {
        static mut instance: ::protobuf::lazy::Lazy<IdentifyClient> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const IdentifyClient,
        };
        unsafe {
            instance.get(IdentifyClient::new)
        }
    }

    // required int32 version = 1;

    pub fn clear_version(&mut self) {
        self.version = ::std::option::Option::None;
    }

    pub fn has_version(&self) -> bool {
        self.version.is_some()
    }

    // Param is passed by value, moved
    pub fn set_version(&mut self, v: i32) {
        self.version = ::std::option::Option::Some(v);
    }

    pub fn get_version(&self) -> i32 {
        self.version.unwrap_or(0)
    }

    fn get_version_for_reflect(&self) -> &::std::option::Option<i32> {
        &self.version
    }

    fn mut_version_for_reflect(&mut self) -> &mut ::std::option::Option<i32> {
        &mut self.version
    }

    // optional string connection_name = 2;

    pub fn clear_connection_name(&mut self) {
        self.connection_name.clear();
    }

    pub fn has_connection_name(&self) -> bool {
        self.connection_name.is_some()
    }

    // Param is passed by value, moved
    pub fn set_connection_name(&mut self, v: ::std::string::String) {
        self.connection_name = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_connection_name(&mut self) -> &mut ::std::string::String {
        if self.connection_name.is_none() {
            self.connection_name.set_default();
        }
        self.connection_name.as_mut().unwrap()
    }

    // Take field
    pub fn take_connection_name(&mut self) -> ::std::string::String {
        self.connection_name.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_connection_name(&self) -> &str {
        match self.connection_name.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_connection_name_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.connection_name
    }

    fn mut_connection_name_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.connection_name
    }
}

impl ::protobuf::Message for IdentifyClient {
    fn is_initialized(&self) -> bool {
        if self.version.is_none() {
            return false;
        }
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_int32()?;
                    self.version = ::std::option::Option::Some(tmp);
                },
                2 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.connection_name)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.version {
            my_size += ::protobuf::rt::value_size(1, v, ::protobuf::wire_format::WireTypeVarint);
        }
        if let Some(ref v) = self.connection_name.as_ref() {
            my_size += ::protobuf::rt::string_size(2, &v);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.version {
            os.write_int32(1, v)?;
        }
        if let Some(ref v) = self.connection_name.as_ref() {
            os.write_string(2, &v)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for IdentifyClient {
    fn new() -> IdentifyClient {
        IdentifyClient::new()
    }

    fn descriptor_static(_: ::std::option::Option<IdentifyClient>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeInt32>(
                    "version",
                    IdentifyClient::get_version_for_reflect,
                    IdentifyClient::mut_version_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "connection_name",
                    IdentifyClient::get_connection_name_for_reflect,
                    IdentifyClient::mut_connection_name_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<IdentifyClient>(
                    "IdentifyClient",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for IdentifyClient {
    fn clear(&mut self) {
        self.clear_version();
        self.clear_connection_name();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for IdentifyClient {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for IdentifyClient {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct ClientIdentified {
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for ClientIdentified {}

impl ClientIdentified {
    pub fn new() -> ClientIdentified {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static ClientIdentified {
        static mut instance: ::protobuf::lazy::Lazy<ClientIdentified> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ClientIdentified,
        };
        unsafe {
            instance.get(ClientIdentified::new)
        }
    }
}

impl ::protobuf::Message for ClientIdentified {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for ClientIdentified {
    fn new() -> ClientIdentified {
        ClientIdentified::new()
    }

    fn descriptor_static(_: ::std::option::Option<ClientIdentified>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let fields = ::std::vec::Vec::new();
                ::protobuf::reflect::MessageDescriptor::new::<ClientIdentified>(
                    "ClientIdentified",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for ClientIdentified {
    fn clear(&mut self) {
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for ClientIdentified {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for ClientIdentified {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(Clone,PartialEq,Eq,Debug,Hash)]
pub enum OperationResult {
    Success = 0,
    PrepareTimeout = 1,
    CommitTimeout = 2,
    ForwardTimeout = 3,
    WrongExpectedVersion = 4,
    StreamDeleted = 5,
    InvalidTransaction = 6,
    AccessDenied = 7,
}

impl ::protobuf::ProtobufEnum for OperationResult {
    fn value(&self) -> i32 {
        *self as i32
    }

    fn from_i32(value: i32) -> ::std::option::Option<OperationResult> {
        match value {
            0 => ::std::option::Option::Some(OperationResult::Success),
            1 => ::std::option::Option::Some(OperationResult::PrepareTimeout),
            2 => ::std::option::Option::Some(OperationResult::CommitTimeout),
            3 => ::std::option::Option::Some(OperationResult::ForwardTimeout),
            4 => ::std::option::Option::Some(OperationResult::WrongExpectedVersion),
            5 => ::std::option::Option::Some(OperationResult::StreamDeleted),
            6 => ::std::option::Option::Some(OperationResult::InvalidTransaction),
            7 => ::std::option::Option::Some(OperationResult::AccessDenied),
            _ => ::std::option::Option::None
        }
    }

    fn values() -> &'static [Self] {
        static values: &'static [OperationResult] = &[
            OperationResult::Success,
            OperationResult::PrepareTimeout,
            OperationResult::CommitTimeout,
            OperationResult::ForwardTimeout,
            OperationResult::WrongExpectedVersion,
            OperationResult::StreamDeleted,
            OperationResult::InvalidTransaction,
            OperationResult::AccessDenied,
        ];
        values
    }

    fn enum_descriptor_static(_: ::std::option::Option<OperationResult>) -> &'static ::protobuf::reflect::EnumDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::EnumDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::EnumDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                ::protobuf::reflect::EnumDescriptor::new("OperationResult", file_descriptor_proto())
            })
        }
    }
}

impl ::std::marker::Copy for OperationResult {
}

impl ::protobuf::reflect::ProtobufValue for OperationResult {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Enum(self.descriptor())
    }
}

static file_descriptor_proto_data: &'static [u8] = b"\
    \n\x0emessages.proto\x12\x08Messages\x1a\x0frustproto.proto\"\x94\x01\n\
    \x08NewEvent\x12\x10\n\x08event_id\x18\x01\x20\x02(\x0c\x12\x12\n\nevent\
    _type\x18\x02\x20\x02(\t\x12\x19\n\x11data_content_type\x18\x03\x20\x02(\
    \x05\x12\x1d\n\x15metadata_content_type\x18\x04\x20\x02(\x05\x12\x0c\n\
    \x04data\x18\x05\x20\x02(\x0c\x12\x10\n\x08metadata\x18\x06\x20\x01(\x0c\
    :\x08\x98\xa7\x08\x01\xa0\xa7\x08\x01\"\xee\x01\n\x0bEventRecord\x12\x17\
    \n\x0fevent_stream_id\x18\x01\x20\x02(\t\x12\x14\n\x0cevent_number\x18\
    \x02\x20\x02(\x03\x12\x10\n\x08event_id\x18\x03\x20\x02(\x0c\x12\x12\n\n\
    event_type\x18\x04\x20\x02(\t\x12\x19\n\x11data_content_type\x18\x05\x20\
    \x02(\x05\x12\x1d\n\x15metadata_content_type\x18\x06\x20\x02(\x05\x12\
    \x0c\n\x04data\x18\x07\x20\x02(\x0c\x12\x10\n\x08metadata\x18\x08\x20\
    \x01(\x0c\x12\x0f\n\x07created\x18\t\x20\x01(\x03\x12\x15\n\rcreated_epo\
    ch\x18\n\x20\x01(\x03:\x08\x98\xa7\x08\x01\xa0\xa7\x08\x01\"a\n\x14Resol\
    vedIndexedEvent\x12$\n\x05event\x18\x01\x20\x02(\x0b2\x15.Messages.Event\
    Record\x12#\n\x04link\x18\x02\x20\x01(\x0b2\x15.Messages.EventRecord\"\
    \x8d\x01\n\rResolvedEvent\x12$\n\x05event\x18\x01\x20\x02(\x0b2\x15.Mess\
    ages.EventRecord\x12#\n\x04link\x18\x02\x20\x01(\x0b2\x15.Messages.Event\
    Record\x12\x17\n\x0fcommit_position\x18\x03\x20\x02(\x03\x12\x18\n\x10pr\
    epare_position\x18\x04\x20\x02(\x03\"\x86\x01\n\x0bWriteEvents\x12\x17\n\
    \x0fevent_stream_id\x18\x01\x20\x02(\t\x12\x18\n\x10expected_version\x18\
    \x02\x20\x02(\x03\x12\"\n\x06events\x18\x03\x20\x03(\x0b2\x12.Messages.N\
    ewEvent\x12\x16\n\x0erequire_master\x18\x04\x20\x02(\x08:\x08\x98\xa7\
    \x08\x01\xa0\xa7\x08\x01\"\xdf\x01\n\x14WriteEventsCompleted\x12)\n\x06r\
    esult\x18\x01\x20\x02(\x0e2\x19.Messages.OperationResult\x12\x0f\n\x07me\
    ssage\x18\x02\x20\x01(\t\x12\x1a\n\x12first_event_number\x18\x03\x20\x02\
    (\x03\x12\x19\n\x11last_event_number\x18\x04\x20\x02(\x03\x12\x18\n\x10p\
    repare_position\x18\x05\x20\x01(\x03\x12\x17\n\x0fcommit_position\x18\
    \x06\x20\x01(\x03\x12\x17\n\x0fcurrent_version\x18\x07\x20\x01(\x03:\x08\
    \x98\xa7\x08\x01\xa0\xa7\x08\x01\"v\n\tReadEvent\x12\x17\n\x0fevent_stre\
    am_id\x18\x01\x20\x02(\t\x12\x14\n\x0cevent_number\x18\x02\x20\x02(\x03\
    \x12\x18\n\x10resolve_link_tos\x18\x03\x20\x02(\x08\x12\x16\n\x0erequire\
    _master\x18\x04\x20\x02(\x08:\x08\xa0\xa7\x08\x01\x98\xa7\x08\x01\"\x86\
    \x02\n\x12ReadEventCompleted\x12<\n\x06result\x18\x01\x20\x02(\x0e2,.Mes\
    sages.ReadEventCompleted.ReadEventResult\x12-\n\x05event\x18\x02\x20\x02\
    (\x0b2\x1e.Messages.ResolvedIndexedEvent\x12\r\n\x05error\x18\x03\x20\
    \x01(\t\"j\n\x0fReadEventResult\x12\x0b\n\x07Success\x10\0\x12\x0c\n\x08\
    NotFound\x10\x01\x12\x0c\n\x08NoStream\x10\x02\x12\x11\n\rStreamDeleted\
    \x10\x03\x12\t\n\x05Error\x10\x04\x12\x10\n\x0cAccessDenied\x10\x05:\x08\
    \xa0\xa7\x08\x01\x98\xa7\x08\x01\"\x95\x01\n\x10ReadStreamEvents\x12\x17\
    \n\x0fevent_stream_id\x18\x01\x20\x02(\t\x12\x19\n\x11from_event_number\
    \x18\x02\x20\x02(\x03\x12\x11\n\tmax_count\x18\x03\x20\x02(\x05\x12\x18\
    \n\x10resolve_link_tos\x18\x04\x20\x02(\x08\x12\x16\n\x0erequire_master\
    \x18\x05\x20\x02(\x08:\x08\xa0\xa7\x08\x01\x98\xa7\x08\x01\"\x88\x03\n\
    \x19ReadStreamEventsCompleted\x12.\n\x06events\x18\x01\x20\x03(\x0b2\x1e\
    .Messages.ResolvedIndexedEvent\x12D\n\x06result\x18\x02\x20\x02(\x0e24.M\
    essages.ReadStreamEventsCompleted.ReadStreamResult\x12\x19\n\x11next_eve\
    nt_number\x18\x03\x20\x02(\x03\x12\x19\n\x11last_event_number\x18\x04\
    \x20\x02(\x03\x12\x18\n\x10is_end_of_stream\x18\x05\x20\x02(\x08\x12\x1c\
    \n\x14last_commit_position\x18\x06\x20\x02(\x03\x12\r\n\x05error\x18\x07\
    \x20\x01(\t\"n\n\x10ReadStreamResult\x12\x0b\n\x07Success\x10\0\x12\x0c\
    \n\x08NoStream\x10\x01\x12\x11\n\rStreamDeleted\x10\x02\x12\x0f\n\x0bNot\
    Modified\x10\x03\x12\t\n\x05Error\x10\x04\x12\x10\n\x0cAccessDenied\x10\
    \x05:\x08\xa0\xa7\x08\x01\x98\xa7\x08\x01\"\xe4\x02\n\nNotHandled\x125\n\
    \x06reason\x18\x01\x20\x02(\x0e2%.Messages.NotHandled.NotHandledReason\
    \x12\x17\n\x0fadditional_info\x18\x02\x20\x01(\x0c\x1a\xc7\x01\n\nMaster\
    Info\x12\x1c\n\x14external_tcp_address\x18\x01\x20\x02(\t\x12\x19\n\x11e\
    xternal_tcp_port\x18\x02\x20\x02(\x05\x12\x1d\n\x15external_http_address\
    \x18\x03\x20\x02(\t\x12\x1a\n\x12external_http_port\x18\x04\x20\x02(\x05\
    \x12#\n\x1bexternal_secure_tcp_address\x18\x05\x20\x01(\t\x12\x20\n\x18e\
    xternal_secure_tcp_port\x18\x06\x20\x01(\x05\"<\n\x10NotHandledReason\
    \x12\x0c\n\x08NotReady\x10\0\x12\x0b\n\x07TooBusy\x10\x01\x12\r\n\tNotMa\
    ster\x10\x02\"\x12\n\x10ScavengeDatabase\"\xdb\x01\n\x19ScavengeDatabase\
    Completed\x12B\n\x06result\x18\x01\x20\x02(\x0e22.Messages.ScavengeDatab\
    aseCompleted.ScavengeResult\x12\r\n\x05error\x18\x02\x20\x01(\t\x12\x15\
    \n\rtotal_time_ms\x18\x03\x20\x02(\x05\x12\x19\n\x11total_space_saved\
    \x18\x04\x20\x02(\x03\"9\n\x0eScavengeResult\x12\x0b\n\x07Success\x10\0\
    \x12\x0e\n\nInProgress\x10\x01\x12\n\n\x06Failed\x10\x02\":\n\x0eIdentif\
    yClient\x12\x0f\n\x07version\x18\x01\x20\x02(\x05\x12\x17\n\x0fconnectio\
    n_name\x18\x02\x20\x01(\t\"\x12\n\x10ClientIdentified*\xb0\x01\n\x0fOper\
    ationResult\x12\x0b\n\x07Success\x10\0\x12\x12\n\x0ePrepareTimeout\x10\
    \x01\x12\x11\n\rCommitTimeout\x10\x02\x12\x12\n\x0eForwardTimeout\x10\
    \x03\x12\x18\n\x14WrongExpectedVersion\x10\x04\x12\x11\n\rStreamDeleted\
    \x10\x05\x12\x16\n\x12InvalidTransaction\x10\x06\x12\x10\n\x0cAccessDeni\
    ed\x10\x07\
";

static mut file_descriptor_proto_lazy: ::protobuf::lazy::Lazy<::protobuf::descriptor::FileDescriptorProto> = ::protobuf::lazy::Lazy {
    lock: ::protobuf::lazy::ONCE_INIT,
    ptr: 0 as *const ::protobuf::descriptor::FileDescriptorProto,
};

fn parse_descriptor_proto() -> ::protobuf::descriptor::FileDescriptorProto {
    ::protobuf::parse_from_bytes(file_descriptor_proto_data).unwrap()
}

pub fn file_descriptor_proto() -> &'static ::protobuf::descriptor::FileDescriptorProto {
    unsafe {
        file_descriptor_proto_lazy.get(|| {
            parse_descriptor_proto()
        })
    }
}
